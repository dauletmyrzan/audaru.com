<div class="h4 my-0">{{ $query }}</div>
@if($translations->count() > 0)
    <div class="h5 mt-0 text-muted mb-2">{{ $langFrom != 'en' ? '['.transliterate($query).']' : '' }}</div>
    @foreach($translations as $key => $subset)
        <div class="translated-word-group mb-4">
            @if($translations->count() > 1)
                <div class="h5 text-info mb-2">{{ $key }}:</div>
            @endif
            @foreach($subset as $word)
                <div class="translated-word" data-id="{{ $word->id }}">
                    @if(\Auth::check() && \Auth::user()->hasRole('admin'))
                        <span class="edit-translated-word text-info" title="Редактировать перевод">
                            <i class="fas fa-pen fa-lg"></i>
                        </span>
                        <span class="delete-translated-word text-danger" title="Удалить перевод">
                            <i class="fas fa-times-circle fa-lg"></i>
                        </span>
                    @endif
                    <div class="font-weight-normal d-inline-block">
                        - <div style="display:inline-block;" class="the-word">{{ $word->$langTo }}</div>
                        @if(\Session::get('locale') == 'kz')
                        <span>{{ $word->topics->count() > 0 ? "(" . implode(', ', $word->topics->pluck('name_kz')->toArray()) . ")" : '(жалпы)' }}</span>
                        @else
                        <span>{{ $word->topics->count() > 0 ? "(" . implode(', ', $word->topics->pluck('name')->toArray()) . ")" : '(general)' }}</span>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
@else
    @lang('main.not_found')
@endif