<div class="loading-layer"></div>
<div class="my-3 px-3">
    <h5 class="font-weight-bold">Всего данных: {{ $translation->count() }}</h5>
</div>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <th>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" name="selected_translations" type="checkbox" id="select_all">
                        <span class="form-check-sign"></span>
                    </label>
                </div>
            </th>
            <th>@sortablelink('en')</th>
            <th>@sortablelink('kz')</th>
            <th>Статус</th>
            <th>@sortablelink('updated_at', 'Дата создания')</th>
            <th>Дата {{ isset($title) && $title !== 'Корзина' ? 'изменения' : 'удаления' }}</th>
        </thead>
        <tbody>
        @if($translation->count() > 0)
            @foreach($translation as $row)
                <tr data-id="{{ $row->id }}" class="translation-row">
                    <td>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="select_translation[]" value="{{ $row->id }}">
                                <span class="form-check-sign"></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ $row->en }}</td>
                    <td>{{ $row->kz }}</td>
                    <td>{!! $row->status() !!}</td>
                    <td>{{ $row->created_at }}</td>
                    <td>
                        @if($row->status(1) == 'На модерации')
                            <button data-id="{{ $row->id }}" class="btn btn-success approve-btn">Утвердить</button>
                            <button data-id="{{ $row->id }}" class="btn btn-secondary decline-btn">Отклонить</button>
                        @endif
                        <a class="btn btn-success btn-sm" href="/admin/translations/edit/{{ $row->id }}">Редактировать</a>
                    </td>
                    <td><div data-id="{{ $row->id }}" class="delete-translation-btn nc-icon nc-simple-remove text-danger font-weight-bold"></div></td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="6" class="text-center">Нет результатов</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<div class="selected-actions">
    <button class="btn btn-danger btn-sm my-3" id="delete_all">Удалить выбранные</button>
</div>