@foreach($stats as $key => $stat)
    <tr>
        <td>{{ $key+1 }}</td>
        <td class="query"><b>{{ $stat->query }}</b></td>
        <td>{{ $stat->remote_ip }}</td>
        <td>{{ App\User::find($stat->user_id) ? App\User::find($stat->user_id)->name : '' }}</td>
        <td><span class="text-muted">{{ $stat->created_at }}</span></td>
        <td><button class="btn btn-success check-translation">Проверить перевод</button></td>
    </tr>
@endforeach