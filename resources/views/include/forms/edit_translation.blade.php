<form action="{{ route('translation_update') }}" method="post" name="edit_translation_form">
    <input type="hidden" value="{{ $translation->id }}" name="id">
    <div class="form-group">
        <label>Казахский</label>
        <input type="text" name="kz" class="form-control" value="{{ $translation->kz }}" required>
    </div>
    <div class="form-group">
        <label>Qazaqsha</label>
        <input type="text" name="qaz" class="form-control" value="{{ $translation->qaz }}" required>
    </div>
    <div class="form-group">
        <label>Английский</label>
        <input type="text" name="en" class="form-control" value="{{ $translation->en }}" required>
    </div>
    <div class="form-group">
        <label>Рейтинг</label>
        <input type="number" min="0" step="1" name="rating" class="form-control" value="{{ $translation->rating }}" required>
    </div>
    <div class="form-group">
        <label for="topics">Тематика</label>
        <select name="topics[]" id="topics" class="form-control" multiple required style="min-height:150px;">
            @foreach($topics as $topic)
                <option value="{{ $topic->id }}" {{ $translation->topics->contains($topic->id) ? 'selected' : '' }}>{{ $topic->name }}</option>
            @endforeach
        </select>
    </div>
</form>