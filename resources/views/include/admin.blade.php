<div class="modal fade" id="editTranslationModal" tabindex="-1" role="dialog" aria-labelledby="editTranslationModalLabel" aria-modal="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Редактирование перевода</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" id="save_edit_translation_btn" class="btn btn-primary">Применить</button>
            </div>
        </div>
    </div>
</div>