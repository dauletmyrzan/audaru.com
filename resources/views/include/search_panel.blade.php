<div id="navbarSearchPanel">
    <div class="container">
        <div class="row">
            <div class="col-md-8 pt-2" style="background-color:#f9f9f9;">
                <div class="row align-items-center justify-content-center mb-3">
                    <div class="col-md-5">
                        <select name="lang_from" id="lang_from" class="form-control form-control-white rounded-0 py-3">
                            <option value="kz" {{ \App::getLocale() == 'kz' ? 'selected' : '' }}>{{ __('main.kazakh') }}</option>
                            <option value="qaz">Qazaqsha</option>
                            <option value="en" {{ \App::getLocale() == 'en' ? 'selected' : '' }}>English</option>
                        </select>
                    </div>
                    <div class="col-md-2 text-center">
                        <button class="btn btn-white switch-lang"><i class="fas fa-exchange-alt"></i></button>
                    </div>
                    <div class="col-md-5">
                        <select name="lang_to" id="lang_to" class="form-control form-control-white rounded-0 py-3">
                            <option value="kz" {{ \App::getLocale() == 'en' ? 'selected' : '' }}>{{ __('main.kazakh') }}</option>
                            <option value="qaz">Qazaqsha</option>
                            <option value="en" {{ \App::getLocale() == 'kz' ? 'selected' : '' }}>English</option>
                        </select>
                    </div>
                </div>
                <div class="row border-bottom" id="search_block_nav">
                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="text" placeholder="Сөзді немесе сөз тіркесін енгізіңіз" class="form-control form-control-white py-3 rounded-0 kazakh-letters search-input">
                            <div class="input-group-append">
                                <span class="input-group-text rounded-0 px-2 show-history" @if(!\Session::has('history')) style="display: none;" @endif title="Посмотреть историю"><i class="fas fa-history"></i></span>
                                <span class="input-group-text rounded-0 px-2 border-left-0 clear-input" title="Очистить"><i class="fas fa-times-circle"></i></span>
                                <span class="input-group-text rounded-0 pl-2 pr-3 keyboard-toggle" title="@if(isset($_COOKIE['keyboard']) && $_COOKIE['keyboard'] == 'visible') Скрыть клавиатуру @else Показать клавиатуру @endif"><i class="fas fa-keyboard"></i></span>
                            </div>
                        </div>
                        <div class="my-2 random-phrases">
                            <span class="random-phrases-kz">Мысалы: {!! implode(', ', $randomPhrases['kz']) !!}</span>
                            <span class="random-phrases-qaz">Mysaly: {!! implode(', ', $randomPhrases['qaz']) !!}</span>
                            <span class="random-phrases-en">Example: {!! implode(', ', $randomPhrases['en']) !!}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button class="btn py-3 m-0 btn-round w-100 translate-btn" style="background-color:{{ $settings->color_scheme }}">Аудару</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>