@extends('layouts.app')

@section('style')
    <style>
        a{
            color: #222;
        }
    </style>
@endsection

@section('content')
    <div class="container mt-100 min-height-85vh">
        {!! \Session::get('locale') == 'kz' ? $page->content_kz : $page->content_en !!}
        <div class="mb-5">
            @php
                $lang = \Session::get('locale') == 'kz';
                $height = \Agent::isMobile() ? 300 : 600;
                $findRoute = $lang == 'kz' ? 'Translators Group аударма агенттігіне жолды табу' : 'Find a way to Translators Group';
                $lookMap = $lang == 'kz' ? 'Алматы картасынан қарау' : 'Look on the Almaty map';
                $photos = $lang == 'kz' ? 'Компания сүреттері' : 'Photos of the company';
            @endphp
            <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940001395566/center/76.95467948913576,43.23522972566861/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">{{ $lookMap }}</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/firm/9429940001395566/photos/9429940001395566/center/76.95467948913576,43.23522972566861/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">{{ $photos }}</a></div><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.954674,43.234934/zoom/16/routeTab/rsType/bus/to/76.954674,43.234934╎Translators Group, агентство переводов?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">{{ $findRoute }}</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":"100%","height":{{ $height }},"borderColor":"transparent","pos":{"lat":43.23522972566861,"lon":76.95467948913576,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"9429940001395566"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
        </div>
    </div>
@endsection