<!doctype html>
<html lang="{{ \Session::get('locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (isset($seo) && $seo)
        {!! $seo !!}
    @else
        @seo
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,500,700" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css?v=3.4') }}" rel="stylesheet">
    <link href="{{ asset('css/now-ui-kit.css?v=3.4') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.css?v=3.4') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css?v=3.4') }}" rel="stylesheet">
    <style>
        a:not('.btn'){
            color: {{ $settings->color_scheme }};
        }
        .footer a:hover{
            color: {{ $settings->color_scheme }}!important;
        }
        .sidebar-collapse .navbar-collapse:before{
            background-color: {{ $settings->color_scheme }};
        }
    </style>
    @yield('style')
</head>
<body class="{{ $bodyClass ?? '' }} sidebar-collapse">
    <div id="app">
        <nav class="navbar navbar-expand-lg fixed-top navbar-dark" color-on-scroll="400" style="background-color:{{ $settings->color_scheme }}">
            <div class="container">
                <div class="navbar-translate">
                    <a class="navbar-brand font-weight-bold" href="/" rel="tooltip" data-placement="bottom">
                        <img src="{{ asset('img/logo.png') }}" alt="Лого" width="30">
                        <span class="ml-2">Audaru.com</span>
                    </a>
                    <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar top-bar"></span>
                        <span class="navbar-toggler-bar middle-bar"></span>
                        <span class="navbar-toggler-bar bottom-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="../assets/img/blurred-image-1.jpg">
                    <ul class="navbar-nav">
                        <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                            <a class="nav-link" href="/">{{ __('main.main') }}</a>
                        </li>
                        <li class="nav-item {{ Request::is('translation/add') ? 'active' : '' }}">
                            <a class="nav-link" href="/translation/add">{{ __('main.add_translation') }}</a>
                        </li>
                        <li class="nav-item {{ Request::is('cards') ? 'active' : '' }}">
                            <a class="nav-link" href="/cards">{{ __('main.training') }}</a>
                        </li>
                        <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
                            <a class="nav-link" href="/about">{{ __('main.about') }}</a>
                        </li>
                        <li class="nav-item {{ Request::is('contacts') ? 'active' : '' }}">
                            <a class="nav-link" href="/contacts">{{ __('main.contacts') }}</a>
                        </li>
                    </ul>
                    <div class="navbar-nav share-bar pr-3"></div>
                    <ul class="navbar-nav pl-0 pl-md-3">
                        @if(\Auth::check())
                        <div class="dropdown button-dropdown">
                            <a class="nav-link active" id="navbarDropdown" data-toggle="dropdown" href="#">
                                <i class="fas fa-user"></i> <b class="ml-1">{{ Auth::user()->name ?? Auth::user()->email }}</b>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('profile') }}">{{ __('main.profile') }}</a>
                                @if(\Auth::user()->hasAnyRole(['admin', 'editor', 'seo']))
                                <a class="dropdown-item" href="{{ route('admin') }}">{{ __('main.admin_panel') }}</a>
                                @endif
                                <a class="dropdown-item" href="/logout">{{ __('main.sign_out') }}</a>
                            </div>
                        </div>
                        @else
                            <li class="nav-item">
                                <a class="nav-link font-weight-bold" href="/login">{{ __('main.sign_in') }}</a>
                            </li>
                        @endif
                        <div class="dropdown button-dropdown">
                            <a class="nav-link active" id="langDropdown" data-toggle="dropdown" href="#">
                                <b class="mr-1">{{ \Session::get('locale') }}</b> <i class="fas fa-globe"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="langDropdown">
                                <a class="dropdown-item {{ \Session::get('locale') === 'en' ? 'font-weight-bold' : '' }}" href="/lang/en">EN</a>
                                <a class="dropdown-item {{ \Session::get('locale') === 'kz' ? 'font-weight-bold' : '' }}" href="/lang/kz">KZ</a>
                            </div>
                        </div>
                    </ul>
                </div>
            </div><br>
        </nav>
        @if(Request::is('/'))
            @include('include.search_panel')
        @endif
        @yield('content')
        @if(!Request::is('about'))
        <div class="section section-download" id="#download-section" data-background-color="black">
            <div class="container">{!! $footer_description !!}</div>
        </div>
        @endif
        <footer class="footer" data-background-color="black">
            <div class="container">
                <nav class="mb-5 text-center text-md-left">
                    <ul>
                        <li>
                            <a href="/">Audaru.com</a>
                        </li>
                        <li>
                            <a href="/about">{{ __('main.about') }}</a>
                        </li>
                        <li>
                            <a href="/translation/add">{{ __('main.add_translation') }}</a>
                        </li>
                        <li>
                            <a href="/cards">{{ __('main.training') }}</a>
                        </li>
                        <li>
                            <a href="/contacts">{{ __('main.contacts') }}</a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright" id="copyright">
                    &copy;
                    <script>
                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                    </script>, {{ __('main.copyright') }}
                </div>
            </div>
            <!--LiveInternet counter--><script type="text/javascript">
                document.write('<a href="//www.liveinternet.ru/click" '+
                    'target="_blank"><img src="//counter.yadro.ru/hit?t12.6;r'+
                    escape(document.referrer)+((typeof(screen)=='undefined')?'':
                        ';s'+screen.width+'*'+screen.height+'*'+(screen.colorDepth?
                        screen.colorDepth:screen.pixelDepth))+';u'+escape(document.URL)+
                    ';h'+escape(document.title.substring(0,150))+';'+Math.random()+
                    '" alt="" title="LiveInternet: показано число просмотров за 24'+
                    ' часа, посетителей за 24 часа и за сегодня" '+
                    'border="0" width="88" height="31"><\/a>')
            </script><!--/LiveInternet-->
        </footer>
    </div>
    <!-- Modals -->
    <div id="notify" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                    </button>
                    <h4 class="title title-up"></h4>
                </div>
                <div class="modal-body pt-3 pb-5 text-center"></div>
            </div>
        </div>
    </div>
    @if(\Auth::check() && \Auth::user()->hasRole('admin'))
        @include('include.admin')
    @endif
    <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{ __('main.report_error') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('report_error') }}" method="post" name="report_error">
                        @csrf
                        <div class="error-div"></div>
                        <input type="hidden" name="selected_text">
                        <input type="hidden" name="id">
                        <div class="form-group">
                            <label>{{ __('main.your_message') }}:</label>
                            <textarea class="form-control border p-2" name="message" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">{{ __('main.send_btn') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="scrollUp" class="btn-round btn-neutral shadow-sm btn-lg px-4 rounded-circle btn-icon">
        <i class="fas fa-chevron-up fa-lg"></i>
    </div>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(56652577, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56652577" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154551373-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-154551373-1');
    </script>

    <!-- Scripts -->
    <script src="{{ asset('js/core/jquery-3.4.1.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/core/popper.min.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/core/bootstrap.min.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/bootstrap-switch.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/nouislider.min.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/now-ui-kit.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/js.cookie-2.2.1.min.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui.min.js?v=3.5') }}" type="text/javascript"></script>
    <script src="{{ asset('js/molly.js?v=3.5') }}"></script>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v5.0"></script>
    @yield('script')
    <script src="{{ asset('js/jquery-social-share-bar.js') }}"></script>
    @if(\Auth::check() && \Auth::user()->hasRole('admin'))
        <script src="{{ asset('js/admin/admin.js?v=3.5') }}"></script>
    @endif
    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $(".alert:not(.alert-permanent)").slideUp();
            }, 3000);
            $("body").on("click", ".report-an-error", function(){
                let id = $(this).parent().data("id");
                if(id === undefined){
                    id = $(this).data('id');
                }
                let reportModal = $("#reportModal");
                reportModal.find('input[name=id]').val(id);
                reportModal.modal('show');
            });
            $("form[name=report_error]").on("submit", function(e){
                e.preventDefault();
                $.ajax({
                    url: '/report_error',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success(response){
                        if(response['success']){
                            $("#reportModal").find('textarea').val('');
                            $("#reportModal").modal('hide');
                            $.notify({
                                body: response['message'],
                                delay: 400
                            });
                        }
                    }
                });
            });
            $('.share-bar.navbar-nav').share({
                title: $("meta[property='og:title']").attr('content'),
                additionalClass: 'nav-link',
                position: 'top'
            });
            $('.share-bar.bottom-bar').share({
                title: $("meta[property='og:title']").attr('content'),
                additionalClass: 'btn btn-neutral btn-icon btn-round',
                position: 'bottom'
            });
            $("#scrollUp").on("click", function(){
                $("html,body").animate({
                    scrollTop: 0
                }, 400);
            });
        });
    </script>
</body>
</html>
