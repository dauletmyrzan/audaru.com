<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard Audaru.com</title>
    <meta name="robots" content="noindex,nofollow">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
    <link href="{{ asset('css/admin/bootstrap.min.css?v=3.1') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/bootstrap-datetimepicker.css?v=3.1') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/paper-dashboard.css?v=3.1') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/app.css?v=3.1') }}" rel="stylesheet">
    @yield('style')
</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="brown" data-active-color="success">
            <div class="logo">
                <a href="/" class="simple-text logo-mini">
                    <div class="logo-image-small">
                        <img src="{{ asset('img/logo.png') }}">
                    </div>
                </a>
                <a href="/" class="simple-text logo-normal">
                    <b>Audaru.com</b>
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li {{ Request::is('/admin') ? "class=active" : '' }}>
                        <a href="/admin">
                            <i class="nc-icon nc-world-2"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    @can ('read dictionary')
                        <li {{ Request::is('admin/translation') ? "class=active" : '' }}>
                            <a href="{{ route('translations') }}">
                                <i class="nc-icon nc-paper"></i>
                                <p>База словаря</p>
                            </a>
                        </li>
                    @endcan
                    @can ('duplicates')
                        <li {{ Request::is('admin/duplicates') ? "class=active" : '' }}>
                            <a href="/admin/duplicates">
                                <i class="nc-icon nc-single-copy-04"></i>
                                <p>Дубликаты</p>
                            </a>
                        </li>
                    @endcan
                    @can ('user-phrases')
                        <li {{ Request::is('admin/user-phrases') ? "class=active" : '' }}>
                            <a href="/admin/user-phrases">
                                <i class="nc-icon nc-paper"></i>
                                <p>Предложенные слова</p>
                            </a>
                        </li>
                    @endcan
                    @can ('read topics')
                        <li {{ Request::is('admin/topics') ? "class=active" : '' }}>
                            <a href="{{ route('topics') }}">
                                <i class="nc-icon nc-single-copy-04"></i>
                                <p>Тематики</p>
                            </a>
                        </li>
                    @endcan
                    @can ('basket')
                        <li {{ Request::is('admin/basket') ? "class=active" : '' }}>
                            <a href="{{ route('basket') }}">
                                <i class="nc-icon nc-basket"></i>
                                <p>Корзина</p>
                            </a>
                        </li>
                    @endcan
                    @can ('read statistics')
                        <li {{ Request::is('admin/stats*') ? "class=active" : '' }}>
                            <a href="{{ route('stats') }}">
                                <i class="nc-icon nc-chart-pie-36"></i>
                                <p>Статистика</p>
                            </a>
                        </li>
                    @endcan
                    @can ('reported-errors')
                        <li {{ Request::is('admin/reported-errors*') ? "class=active" : '' }}>
                            <a href="/admin/reported-errors">
                                <i class="nc-icon nc-alert-circle-i"></i>
                                <p>Ошибки</p>
                            </a>
                        </li>
                    @endcan
                    @can ('journal')
                        <li {{ Request::is('admin/journal*') ? "class=active" : '' }}>
                            <a href="{{ route('journal.index') }}">
                                <i class="nc-icon nc-time-alarm"></i>
                                <p>Журнал действий</p>
                            </a>
                        </li>
                    @endcan
                    @can ('pages')
                        <li {{ Request::is('admin/pages*') ? "class=active" : '' }}>
                            <a href="/admin/pages">
                                <i class="nc-icon nc-paper"></i>
                                <p>Страницы</p>
                            </a>
                        </li>
                    @endcan
                    @can ('read users')
                        <li {{ Request::is('admin/users*') ? "class=active" : '' }}>
                            <a href="{{ route('users') }}">
                                <i class="nc-icon nc-circle-10"></i>
                                <p>Пользователи</p>
                            </a>
                        </li>
                    @endcan
                    @can ('read settings')
                        <li {{ Request::is('admin/settings*') ? "class=active" : '' }}>
                            <a href="{{ route('settings') }}">
                                <i class="nc-icon nc-settings-gear-65"></i>
                                <p>Настройки</p>
                            </a>
                        </li>
                    @endcan
                    @can ('read seo')
                        <li {{ Request::is('admin/seo*') ? "class=active" : '' }}>
                            <a href="{{ route('seo') }}">
                                <i class="nc-icon nc-globe"></i>
                                <p>SEO</p>
                            </a>
                        </li>
                    @endcan
                    <li>
                        <a href="/">
                            <i class="nc-icon nc-minimal-left"></i>
                            <p>Посмотреть сайт</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="">{{ $title ?? 'Dashboard' }}</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown-">
                                <a class="nav-link dropdown-toggle-" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="nc-icon nc-single-02"></i>
                                    <p class="font-weight-bold">
                                        <span class="d-md-block">{{ Auth::user()->name ?? Auth::user()->email }}</span>
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="/profile">Мой профиль</a>
                                    <a class="dropdown-item" href="/logout">Выход</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ asset('js/core/jquery-3.4.1.js?v=3.1') }}"></script>
    <script src="{{ asset('js/admin/core/popper.min.js?v=3.1') }}"></script>
    <script src="{{ asset('js/admin/core/bootstrap.min.js?v=3.1') }}"></script>
    <script src="{{ asset('js/admin/core/moment.min.js?v=3.1') }}"></script>
    <script src="{{ asset('js/admin/plugins/perfect-scrollbar.jquery.min.js?v=3.1') }}"></script>
    <script src="{{ asset('js/admin/plugins/bootstrap-notify.js?v=3.1') }}"></script>
    <script src="{{ asset('js/admin/paper-dashboard.min.js?v=3.1') }}" type="text/javascript"></script>
    <script src="{{ asset('js/admin/bootstrap-datetimepicker.js?v=3.1') }}"></script>
    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $('.alert:not(.alert-permanent)').slideUp();
            }, 3000);
            $('#start_date').datetimepicker({
                format: 'YYYY-MM-DD',
                maxDate: moment()
            }).on('dp.change', function(e){
                $('#end_date').data("DateTimePicker").minDate(e.date)
            });
            $('#end_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $(".check-translation").on("click", function(){
                let query = $(this).closest('tr').find('.query').text();
                $.ajax({
                    url: '/admin/checkTranslation',
                    data: {
                        query: query
                    },
                    dataType: 'json',
                    beforeSend(){
                        $('.check-translation').addClass('disabled').prop('disabled', true);
                    },
                    success(response){
                        $.notify({
                            message: response['message']
                        },{
                            type: response['success'] ? 'success' : 'danger',
                            delay: 100,
                            timer: 3000
                        });
                        $('.check-translation').removeClass('disabled').prop('disabled', false);
                    }
                });
            });
            @if(\Session::has('message'))
                $.notify({
                    message: '{{ \Session::get('message') }}'
                },{
                    delay: 100,
                    timer: 3000
                });
            @endif
        });
    </script>
    @yield('script')
</body>
</html>