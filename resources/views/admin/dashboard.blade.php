@extends('layouts.admin')

@section('content')
    <style>
        .nc-icon {
            display: inline-block;
        }
        h1 {
            font-size: 25pt;
            font-weight: 600;
            text-align: center;
            font-family: sans-serif;
        }
        .tile-p {
            font-size: 15pt;
            display: inline-block;
            margin-bottom: 0;
        }
        .tiles a {
            color: #222;
            text-decoration: none;
            display: block;
            background-color: #f9f9f9;
            padding: 60px 0;
            text-align: center;
        }
        .tiles a:hover {
            background-color: #eee;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="card demo-icons">
                <div class="card-header pt-5">
                    <h1>Панель администратора</h1>
                </div>
                <div class="card-body pb-5">
                    <div class="row tiles mx-2 mb-4">
                        @can ('read dictionary')
                            <div class="col-md-3 mb-5">
                                <a href="/admin/translation">
                                    <i class="nc-icon nc-paper"></i>
                                    <p class="tile-p">База словаря</p>
                                </a>
                            </div>
                        @endcan
                        @can ('duplicates')
                            <div class="col-md-3 mb-5">
                                <a href="/admin/duplicates">
                                    <i class="nc-icon nc-single-copy-04"></i>
                                    <p class="tile-p">Дубликаты</p>
                                </a>
                            </div>
                        @endcan
                        @can ('user-phrases')
                            <div class="col-md-3 mb-5">
                                <a href="/admin/user-phrases">
                                    <i class="nc-icon nc-paper"></i>
                                    <p class="tile-p">Предложенные слова</p>
                                </a>
                            </div>
                        @endcan
                        @can ('read topics')
                            <div class="col-md-3 mb-5">
                                <a href="{{ route('topics') }}">
                                    <i class="nc-icon nc-single-copy-04"></i>
                                    <p class="tile-p">Тематики</p>
                                </a>
                            </div>
                        @endcan
                        @can ('basket')
                             <div class="col-md-3 mb-5">
                                <a href="{{ route('basket') }}">
                                    <i class="nc-icon nc-basket"></i>
                                    <p class="tile-p">Корзина</p>
                                </a>
                            </div>
                        @endcan
                        @can ('read statistics')
                             <div class="col-md-3 mb-5">
                                <a href="{{ route('stats') }}">
                                    <i class="nc-icon nc-chart-pie-36"></i>
                                    <p class="tile-p">Статистика</p>
                                </a>
                            </div>
                        @endcan
                        @can ('errors')
                            <div class="col-md-3 mb-5">
                                <a href="/admin/reported-errors">
                                    <i class="nc-icon nc-alert-circle-i"></i>
                                    <p class="tile-p">Ошибки</p>
                                </a>
                            </div>
                        @endcan
                        @can ('journal')
                             <div class="col-md-3 mb-5">
                                <a href="{{ route('journal.index') }}">
                                    <i class="nc-icon nc-time-alarm"></i>
                                    <p class="tile-p">Журнал действий</p>
                                </a>
                            </div>
                        @endcan
                        @can ('pages')
                            <div class="col-md-3 mb-5">
                                <a href="/admin/pages">
                                    <i class="nc-icon nc-paper"></i>
                                    <p class="tile-p">Страницы</p>
                                </a>
                            </div>
                        @endcan
                        @can ('read users')
                             <div class="col-md-3 mb-5">
                                <a href="{{ route('users') }}">
                                    <i class="nc-icon nc-circle-10"></i>
                                    <p class="tile-p">Пользователи</p>
                                </a>
                            </div>
                        @endcan
                        @can ('read settings')
                             <div class="col-md-3 mb-5">
                                <a href="{{ route('settings') }}">
                                    <i class="nc-icon nc-settings-gear-65"></i>
                                    <p class="tile-p">Настройки</p>
                                </a>
                            </div>
                        @endcan
                        @can ('read seo')
                            <div class="col-md-3 mb-5">
                                <a href="{{ route('seo') }}">
                                    <i class="nc-icon nc-globe"></i>
                                    <p class="tile-p">SEO</p>
                                </a>
                            </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection