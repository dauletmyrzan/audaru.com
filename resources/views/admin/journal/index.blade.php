@extends('layouts.admin')

@section('content')
    <div class="content">
        <div class="card-header">
            <form action="{{ route('journal.clear') }}" method="post">
                @csrf
                <div>Очистить данные старше 6 месяцев:</div>
                <button class="btn btn-danger btn-round">Очистить</button>
            </form>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>#</th>
                            <th>Перевод</th>
                            <th>KZ</th>
                            <th>EN</th>
                            <th>QAZ</th>
                            <th>Автор</th>
                            <th>Рейтинг</th>
                            <th>Статус</th>
                            <th>Удаление?</th>
                            <th>Дата события</th>
                        </thead>
                        <tbody>
                        @foreach($events as $key => $event)
                            <tr>
                                <td>{{ $events->firstItem() + $key }}</td>
                                <td><a href="/admin/translations/edit/{{ $event->translation_id }}">Перейти к переводу</a></td>
                                <td>{{ $event->kz }}</td>
                                <td>{{ $event->en }}</td>
                                <td>{{ $event->qaz }}</td>
                                <td><a href="/admin/users/{{ $event->author->id }}">{{ $event->author->name }}</a></td>
                                <td>{{ $event->rating }}</td>
                                <td>{{ $event->status->name }}</td>
                                <td>{{ $event->deleted ? 'Удалено' : 'Нет' }}</td>
                                <td>{{ $event->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $events->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection