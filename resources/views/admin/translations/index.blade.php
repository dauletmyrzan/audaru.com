@extends('layouts.admin')

@php
$selectedTopic = $_GET['topic'] ?? '';
$selectedStatus = $_GET['status'] ?? '';
$query = $_GET['query'] ?? '';
@endphp

@section('style')
    <style>
        #translationTableCard{
            position: relative;
        }
        .loading-layer{
            display: none;
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            top: 0;
            background-color: rgba(255, 255, 255, .8);
            z-index: 100;
            background-image: url('{{ asset('img/ajax-loader.gif') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: 50px;
            opacity: .7;
        }
    </style>
@endsection

@section('content')
    @if(request()->getRequestUri() !== '/admin/user-phrases')
        <div class="row mb-3">
            <div class="col-md-6">
                <form action="" class="form-inline" id="searchForm">
                    <div class="form-group mr-2">
                        <input type="text" name="query" value="{{ $query }}" class="form-control" id="searchInput" placeholder="Поиск..." style="min-width:250px;">
                    </div>

                    <button class="btn btn-primary btn-round btn-lg">Поиск</button>
                </form>
            </div>
            <div class="col-md-6 text-right">
                <a href="/admin/translation/import" class="btn btn-round btn-lg btn-behance"><span class="nc-icon nc-cloud-upload-94"></span> Загрузить словарь</a>
                <a href="/admin/topics/export" class="btn btn-round btn-lg btn-youtube"><span class="nc-icon nc-cloud-download-93"></span> Скачать словарь</a>
                <a href="/admin/translation/create" class="btn btn-round btn-lg btn-success">Добавить слово</a>
            </div>
        </div>
        <hr>
    @endif
    <div class="row mb-3">
        <div class="col-md-8">
            <form action="" class="form-inline">
                <select name="topic" id="topic" class="form-control mt-2 mr-3" onchange="this.form.submit()">
                    <option value="all">Все тематики</option>
                    @foreach($topics as $topic)
                        <option value="{{ $topic->id }}" {{ $selectedTopic == $topic->id ? 'selected' : '' }}>{{ $topic->name }}</option>
                    @endforeach
                </select>
            </form>
            @if (is_numeric($selectedTopic))
                <button class="btn btn-danger btn-round btn-lg" id="delete-topic-dict" data-topic-id="{{ $selectedTopic }}">Удалить словарь по этой теме</button>
            @endif
        </div>
    </div>
    <div class="card" id="translationTableCard">
        <div class="loading-layer"></div>
        <div class="my-3 px-3">
            <h5 class="font-weight-bold">Всего данных: {{ $translation->total() }}</h5>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <th>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="selected_translations" type="checkbox" id="select_all">
                                <span class="form-check-sign"></span>
                            </label>
                        </div>
                    </th>
                    <th>#</th>
                    <th>@sortablelink('en')</th>
                    <th>@sortablelink('kz')</th>
                    <th>Статус</th>
                    <th>@sortablelink('updated_at', 'Дата создания')</th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                @foreach($translation as $key => $row)
                    <tr data-id="{{ $row->id }}" class="translation-row">
                        <td>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="select_translation[]" value="{{ $row->id }}">
                                    <span class="form-check-sign"></span>
                                </label>
                            </div>
                        </td>
                        <td>{{ $translation->firstItem() + $key }}</td>
                        <td>{{ $row->en }}</td>
                        <td>{{ $row->kz }}</td>
                        <td>{!! $row->status() !!}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>
                            @if($row->status(1) == 'На модерации')
                            <button data-id="{{ $row->id }}" class="btn btn-sm btn-success approve-btn">Утвердить</button>
                            <button data-id="{{ $row->id }}" class="btn btn-sm btn-secondary decline-btn">Отклонить</button>
                            @endif
                            <a class="btn btn-success btn-sm" href="/admin/translations/edit/{{ $row->id }}">Редактировать</a>
                        </td>
                        <td><div data-id="{{ $row->id }}" class="delete-translation-btn nc-icon nc-simple-remove text-danger font-weight-bold"></div></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if($translation && $translation->count() > 0)
                {{ $translation->appends(request()->input())->links() }}
            @endif
            <div class="selected-actions">
                <button class="btn btn-danger btn-sm my-3" id="delete_all">Удалить выбранные</button>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('.delete-translation-btn').on('click', function () {
                if(confirm('Вы действительно хотите удалить фразу?')){
                    let thisRow = $(this).closest('tr');
                    $.ajax({
                        url: '/admin/translations/delete',
                        type: 'POST',
                        data: {
                            id: $(this).data('id')
                        },
                        dataType: 'json',
                        success(response){
                            if(response['success']){
                                thisRow.remove();
                            }
                        }
                    });
                }
            });
            $('.approve-btn').on('click', function(){
                $.ajax({
                    url: '/admin/translations/approve',
                    type: 'POST',
                    data: {
                        id: $(this).data('id')
                    },
                    dataType: 'json',
                    success(response){
                        if(response['success']){
                            location.reload();
                        }
                    }
                });
            });
            $('.decline-btn').on('click', function(){
                $.ajax({
                    url: '/admin/translations/decline',
                    type: 'POST',
                    data: {
                        id: $(this).data('id')
                    },
                    dataType: 'json',
                    success(response){
                        if(response['success']){
                            location.reload();
                        }
                    }
                });
            });
            $("input[name=select_translation\\[\\]]").on('change', function(){
                $(".selected-actions").show();
            });
            $("#select_all").on("click", function(){
                let checkBoxes = $("input[name=select_translation\\[\\]]");

                if($(this).prop('checked')){
                    checkBoxes.prop("checked", true);
                    $(".selected-actions").show();
                }else{
                    checkBoxes.prop("checked", false);
                    $(".selected-actions").hide();
                }
            });
            $("#delete_all").on("click", function(){
                if(confirm("Вы действительно хотите удалить?")){
                    let selected = [];
                    $.each($('input[name=select_translation\\[\\]]:checked'), function(){
                        selected.push($(this).val());
                    });
                    $.ajax({
                        url: '/admin/translations/delete',
                        type: 'POST',
                        data: {
                            ids: selected
                        },
                        dataType: 'json',
                        success(response){
                            if(response['success']){
                                location.reload();
                            }
                        }
                    });
                }
            });
            $("#delete-topic-dict").on('click', function () {
                if (confirm('Вы действительно хотите удалить словарь данной темы?')) {
                    let topicId = $(this).data('topic-id');

                    $.ajax({
                        url: '/admin/topics/delete-with-translations',
                        type: 'POST',
                        data: {
                            id: topicId,
                        },
                        success (response) {
                            if (response['success']) {
                                alert('Словарь по данной теме удалена.');

                                location.reload();
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection