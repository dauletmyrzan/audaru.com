@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('translation_store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="kz">Казахский</label>
                    <input type="text" name="kz" id="kz" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="en">Английский</label>
                    <input type="text" name="en" id="en" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="qaz">Qazaqsha</label>
                    <input type="text" name="qaz" id="qaz" class="form-control">
                </div>
                <div class="form-group">
                    <label for="rating">Рейтинг</label>
                    <input type="number" min="0" step="1" id="rating" name="rating" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="topics">Тематика</label>
                    <select name="topics[]" id="topics" class="form-control" multiple required style="min-height:150px;">
                        @foreach($topics as $topic)
                            <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                        @endforeach
                    </select>
                </div>
                <a href="/admin" class="btn btn-light btn-round btn-lg">Назад</a>
                <button type="submit" class="btn btn-primary btn-round btn-lg">Добавить</button>
            </form>
            @if($errors->any())
                <div class="alert alert- alert-permanent">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#kz").on("keyup", function(){
                let word = $(this).val();
                $.ajax({
                    url: '/transliterateAjax',
                    data: {
                        word: word
                    },
                    dataType: 'json',
                    success(response){
                        if(response['success']){
                            $("#qaz").val(response['transliterated']);
                        }
                    }
                });
            });
        });
    </script>
@endsection