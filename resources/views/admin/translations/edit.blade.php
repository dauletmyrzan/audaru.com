@extends('layouts.admin')

@section('style')
    <link rel="stylesheet" href="https://unpkg.com/multiple-select@1.5.2/dist/multiple-select.min.css">
    <style>
        .ms-parent {
            width: 100% !important;
        }

        .ms-drop ul>li {
            padding: 1rem 8px;
        }
        .ms-drop ul>li:hover {
            background-color: #eee;
        }

        .ms-choice {
            height: 40px;
            line-height: 40px;
        }
    </style>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('translation_update') }}" method="post">
                        @csrf
                        <input type="hidden" value="{{ url()->previous() }}" name="redirect">
                        <input type="hidden" value="{{ $phrase->id }}" name="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="kz">Казахский</label>
                                    <input type="text" id="kz" name="kz" value="{{ $phrase->kz }}" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="en">Английский</label>
                                    <input type="text" id="en" name="en" value="{{ $phrase->en }}" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="qaz">Qazaqsha</label>
                                    <input type="text" name="qaz" id="qaz" value="{{ $phrase->qaz }}" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="rating">Рейтинг</label>
                                    <input type="number" min="0" step="1" id="rating" name="rating" value="{{ $phrase->rating }}" class="form-control" required>
                                </div>
                                @if ($phrase->status_id == 4)
                                    <input type="hidden" name="status" value="1">
                                @else
                                    <div class="form-group">
                                        <label for="status">Статус</label>
                                        <select name="status" id="status">
                                            @foreach($statuses as $status)
                                                <option value="{{ $status->id }}" {{ $phrase->status_id == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="topics">Тематика</label>
                                    <select name="topics[]" id="topics" multiple required style="min-height:450px;">
                                        @foreach($topics as $topic)
                                            <option value="{{ $topic->id }}" {{ $phrase->topics->contains($topic->id) ? 'selected' : '' }}>{{ $topic->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <a href="{{ url()->previous() }}" class="btn btn-light btn-round btn-lg">Назад</a>
                        <button type="submit" class="btn btn-primary btn-round btn-lg">@if ($phrase->status_id == 4) Восстановить @else Сохранить @endif</button>
                    </form>
                    @if($errors->any())
                        <div class="alert alert- alert-permanent">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://unpkg.com/multiple-select@1.5.2/dist/multiple-select.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#kz").on("keyup", function(){
                let word = $(this).val();
                $.ajax({
                    url: '/transliterateAjax',
                    data: {
                        word: word
                    },
                    dataType: 'json',
                    success(response){
                        if(response['success']){
                            $("#qaz").val(response['transliterated']);
                        }
                    }
                });
            });
            $('select').multipleSelect({
                displayTitle: true,
                maxHeight: 300,
                minimumCountSelected: 15,
                selectAll: false
            });
        });
    </script>
@endsection