@extends('layouts.admin')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('user.update') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <div class="form-group">
                                <label for="name">Имя</label>
                                <input type="text" id="name" name="name" class="form-control" required value="{{ $user->name }}">
                            </div>
                            <div class="form-group">
                                <label for="surname">Фамилия</label>
                                <input type="text" id="surname" name="surname" class="form-control" value="{{ $user->surname }}">
                            </div>
                            <div class="form-group">
                                <label for="roles">Роли</label>
                                <select name="roles[]" id="roles" class="form-control" multiple>
                                    <option value="no">Не выбрано</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}" {{ $user->hasRole($role->name) ? 'selected' : '' }}>{{ $role->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" id="email" name="email" class="form-control" required readonly value="{{ $user->email }}">
                            </div>
                            <a href="{{ url()->previous() != url()->current() ? url()->previous() : '/admin/users' }}" class="btn btn-secondary btn-round btn-lg">Назад</a>
                            <button class="btn btn-primary btn-round btn-lg">Сохранить</button>
                        </form>
                        <hr>
                        <form action="/admin/deleteUser" method="POST">
                        	@csrf
                        	<input type="hidden" name="id" value="{{ $user->id }}">
                        	<button class="btn btn-danger btn-round btn-lg" onclick="return confirm('Вы уверены?')">Удалить пользователя</button>
                        </form>
                        <a href="/authById/{{ $user->id }}" style="display:none">Авторизоваться</a>
                        @if($errors->any())
                            <div class="alert mb-0 alert-danger alert-permanent">
                                <ul class="mb-0">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="permissions">Права:</label>
                            <select class="form-control" disabled multiple style="min-height:470px;">
                                <option value="no">Не выбрано</option>
                                @foreach($permissions as $permission)
                                    <option value="{{ $permission->id }}" {{ $user->can($permission->name) ? 'selected' : '' }}>{{ $permission->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
