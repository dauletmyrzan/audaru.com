@extends('layouts.admin')

@php
    $selectedRole = $_GET['role'] ?? '';
@endphp

@section('content')
    <div class="content">
        <div class="row mb-3">
            <div class="col-md-4">
                <form action="" class="form-inline">
                    <select name="role" id="role" class="form-control mt-2 mr-3" onchange="this.form.submit()">
                        <option value="all">Все роли</option>
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}" {{ $selectedRole == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                        @endforeach
                    </select>
                </form>
            </div>
            <div class="col-md-8 text-right">
                <a href="{{ route('roles') }}" class="btn btn-danger btn-round btn-lg">Управление ролями</a>
                <a href="{{ route('permissions') }}" class="btn btn-primary btn-round btn-lg">Список прав</a>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="selected-actions">
                    <button class="btn btn-danger btn-sm my-3" id="delete_all">Удалить выбранные</button>
                </div>
                <div class="my-3">
                    <h5 class="font-weight-bold">Всего данных: {{ $users->total() }}</h5>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="selected_users" type="checkbox" id="select_all">
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                            </th>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Дата регистрации</th>
                            <th>Роль</th>
                        </thead>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="selected_users[]" value="{{ $user->id }}">
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                </td>
                                <td>{{ $user->id }}</td>
                                <td><a href="/admin/users/{{ $user->id }}">{{ $user->name }}</a></td>
                                <td>{{ $user->surname }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->getRoleNames()->implode(', ') }}</td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $users->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("input[name=selected_users\\[\\]]").on('change', function() {
                $(".selected-actions").show();
            });

            $("#select_all").on("click", function() {
                let checkBoxes = $("input[name=selected_users\\[\\]]");

                if ($(this).prop('checked')) {
                    checkBoxes.prop("checked", true);
                    $(".selected-actions").show();
                } else {
                    checkBoxes.prop("checked", false);
                    $(".selected-actions").hide();
                }
            });

            $("#delete_all").on("click", function() {
                let selected = [];

                $.each($('input[name=selected_users\\[\\]]:checked'), function() {
                    selected.push($(this).val());
                });

                if (confirm("Вы действительно хотите удалить (" + selected.length + " шт.)?")) {
                    $.ajax({
                        url: '/admin/users/delete',
                        type: 'POST',
                        data: {
                            ids: selected
                        },
                        dataType: 'json',
                        success (response) {
                            if (response['success']) {
                                location.reload();
                            } else {
                                alert(response['message']);
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection