@extends('layouts.admin')

@section('content')
    <div class="card rounded-0">
        <div class="card-header border-bottom">
            <h2 class="card-title font-weight-bold">Настройки</h2>
        </div>
        <div class="card-body pt-0">
            <form action="/admin/settings/update" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="h3 font-weight-bold mb-0">SEO</div>
                        <div class="form-group">
                            <label>Заголовок сайта (KZ)</label>
                            <input type="text" class="form-control" name="title" value="{{ $settings->title }}" required>
                        </div>
                        <div class="form-group">
                            <label>Заголовок сайта (EN)</label>
                            <input type="text" class="form-control" name="title_en" value="{{ $settings->title_en }}" required>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова (KZ)</label>
                            <input type="text" class="form-control" name="keywords" value="{{ $settings->keywords }}" required>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова (EN)</label>
                            <input type="text" class="form-control" name="keywords_en" value="{{ $settings->keywords_en }}" required>
                        </div>
                        <div class="form-group">
                            <label>Описание</label>
                            <textarea class="form-control" name="description" required>{{ $settings->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Описание (EN)</label>
                            <textarea class="form-control" name="description_en" required>{{ $settings->description_en }}</textarea>
                        </div>
                        <div>
                            <label class="d-block">Изображение</label>
                            @if(file_exists('uploads/img/'.$settings->image_src))
                            <img src="{{ asset('uploads/img/' . $settings->image_src) }}" width="250" class="d-block img-thumbnail">
                            @endif
                            <input type="file" name="image_src">
                        </div>
                        <div class="form-group mt-3">
                            <label>URL сайта:</label>
                            <input type="text" class="form-control" name="url" value="{{ $settings->url }}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Выберите цветовую схему сайта:</label>
                        <div class="radio">
                            <input type="radio" name="color_scheme" id="scheme1" value="#2ca8ff" {{ $settings->color_scheme == '#2ca8ff' ? 'checked' : '' }}>
                            <label for="scheme1">
                                <span class="px-3" style="background-color:#2ca8ff;"></span>
                            </label>
                        </div>
                        <div class="radio">
                            <input type="radio" name="color_scheme" id="scheme2" value="#f96332" {{ $settings->color_scheme == '#f96332' ? 'checked' : '' }}>
                            <label for="scheme2">
                                <span class="px-3" style="background-color:#ffb236;"></span>
                            </label>
                        </div>
                        <div class="radio">
                            <input type="radio" name="color_scheme" id="scheme3" value="#ff3636" {{ $settings->color_scheme == '#ff3636' ? 'checked' : '' }}>
                            <label for="scheme3">
                                <span class="px-3" style="background-color:#ff3636;"></span>
                            </label>
                        </div>
                        <div class="radio">
                            <input type="radio" name="color_scheme" id="scheme4" value="#8bc34a" {{ $settings->color_scheme == '#8bc34a' ? 'checked' : '' }}>
                            <label for="scheme4">
                                <span class="px-3" style="background-color:#8bc34a;"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-round btn-lg">Сохранить</button>
            </form>
        </div>
    </div>
@endsection