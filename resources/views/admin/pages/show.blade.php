@extends('layouts.admin')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <style>
        .note-popover{
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="card rounded-0" id="translationTableCard">
        <div class="card-header">
            <div class="h3 font-weight-bold">{{ $page->title_kz }}</div>
        </div>
        <div class="card-body">
            <form action="/admin/pages/update" method="post">
                @csrf
                <input type="hidden" name="id" value="{{ $page->id }}">
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title_kz">Название на казахском:</label>
                            <input type="text" class="form-control" name="title_kz" id="title_kz" value="{{ $page->title_kz }}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title_en">Название на английском:</label>
                            <input type="text" class="form-control" name="title_en" id="title_en" value="{{ $page->title_en }}" required>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="content_kz">Содержимое на казахском:</label>
                            <textarea name="content_kz" class="summernote" id="content_kz">{{ $page->content_kz }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="content_en">Название на английском</label>
                            <textarea name="content_en" class="summernote" id="content_en">{{ $page->content_en }}</textarea>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary">Сохранить</button>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote();
        });
    </script>
@endsection