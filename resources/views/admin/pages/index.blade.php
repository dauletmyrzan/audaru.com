@extends('layouts.admin')

@section('content')
    <div class="card rounded-0">
        <div class="card-header">
            <div class="h3 font-weight-bold">Страницы</div>
        </div>
        <div class="card-body">
            <a href="/admin/pages/about" class="btn btn-info d-inline-block">Редактировать страницу «О компании»</a>
            <a href="/admin/pages/contacts" class="btn btn-info d-inline-block">Редактировать страницу «Контакты»</a>
            <a href="/admin/pages/footer" class="btn btn-info d-inline-block">Редактировать нижний текст</a>
        </div>
    </div>
@endsection