@extends('layouts.admin')

@section('content')
    <div class="card rounded-0">
        <div class="card-header border-bottom">
            <a href="/admin/users" class="btn btn-secondary btn-round btn-lg">Назад</a>
            <h2 class="card-title font-weight-bold">Права</h2>
        </div>
        <div class="card-body pt-0">
            <table class="table table-hover">
                <thead>
                    <th style="width:100px;">#</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th></th>
                </thead>
                @foreach ($permissions as $permission)
                    <tr>
                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->title }}</td>
                        <td>{{ $permission->description }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection