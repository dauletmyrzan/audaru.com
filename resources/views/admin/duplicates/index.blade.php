@extends('layouts.admin')

@php
  $selectedLang = $_GET['language'] ?? 'en';
@endphp

@section('content')
    <div class="card rounded-0">
        <div class="card-header border-bottom">
            <h2 class="card-title font-weight-bold">Слова-дубли</h2>
        </div>
        <div class="card-body pt-0">
            <table class="table table-hover">
                <thead>
                    <th>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="selected_translations" type="checkbox" id="select_all">
                                <span class="form-check-sign"></span>
                            </label>
                        </div>
                    </th>
                    <th style="width:100px;">ID</th>
                    <th>EN</th>
                    <th>KZ</th>
                    <th></th>
                </thead>
                @foreach ($duplicates as $row)
                    <tr>
                        <td>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="select_translation[]" value="{{ $row->id }}">
                                    <span class="form-check-sign"></span>
                                </label>
                            </div>
                        </td>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->en }}</td>
                        <td>{{ $row->kz }}</td>
                        <td>
                            <a class="btn btn-success btn-sm" href="/admin/translations/edit/{{ $row->id }}">Редактировать</a>
                            <button class="btn btn-danger btn-sm delete-translation-btn" data-id="{{ $row->id }}">Удалить</button>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $duplicates->appends(request()->input())->links() }}
        </div>
        <div class="card-footer">
            <div class="selected-actions">
                <button class="btn btn-danger btn-sm my-3" id="delete_all">Удалить выбранные</button>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.delete-translation-btn').on('click', function () {
                if(confirm('Вы действительно хотите удалить фразу?')){
                    let thisRow = $(this).closest('tr');

                    $.ajax({
                        url: '/admin/translations/delete',
                        type: 'POST',
                        data: {
                            id: $(this).data('id')
                        },
                        dataType: 'json',
                        success(response){
                            if(response['success']){
                                thisRow.remove();
                            }
                        }
                    });
                }
            });

            $("input[name=select_translation\\[\\]]").on('change', function(){
                $(".selected-actions").show();
            });
            $("#select_all").on("click", function(){
                let checkBoxes = $("input[name=select_translation\\[\\]]");

                if($(this).prop('checked')){
                    checkBoxes.prop("checked", true);
                    $(".selected-actions").show();
                }else{
                    checkBoxes.prop("checked", false);
                    $(".selected-actions").hide();
                }
            });
            $("#delete_all").on("click", function(){
                if(confirm("Вы действительно хотите удалить?")){
                    let selected = [];
                    $.each($('input[name=select_translation\\[\\]]:checked'), function(){
                        selected.push($(this).val());
                    });
                    $.ajax({
                        url: '/admin/translations/delete',
                        type: 'POST',
                        data: {
                            ids: selected
                        },
                        dataType: 'json',
                        success(response){
                            if(response['success']){
                                // location.reload();
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection