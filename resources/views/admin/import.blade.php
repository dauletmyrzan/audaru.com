@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header border-bottom">
            <h3 class="card-title">Импорт базы переводов</h3>
        </div>
        <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-12">
                    @if(isset($path))
                        <div class="text-center">
                            <div class="text-center text-muted font-weight-bold">{{ $fileName }}</div>
                            <div id="loading" class="lds-ring my-3"><div></div><div></div><div></div><div></div></div>
                            <button class="btn btn-success btn-lg" id="start_import">Начать импорт</button><br>
                            <a href="/admin/translation/import" class="btn-link">Загрузить другой файл</a>
                        </div>
                    @else
                        <form action="{{ route('upload_base') }}" name="upload_base" enctype="multipart/form-data" method="post" class="text-center mb-3">
                            @csrf
                            <div class="mb-4">
                                <div class="h4 font-weight-bold mb-1">Выберите файл для загрузки</div>
                                <div>Файл должен быть в формате <strong>.xslx</strong></div>
                            </div>
                            <div class="mb-4">
                                <input type="file" name="file" required>
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg" id="upload_btn">Загрузить файл</button>
                        </form>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-permanent">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="border-top card-footer">
            <div class="my-2 text-center">
                <a href="{{ asset('Пример%20базы%20переводов.xlsx') }}" download class="text-success h6">Скачать пример файла</a>
            </div>
        </div>
    </div>
@endsection

@if(isset($path))
@section('script')
    <script>
        $(document).ready(function(){
            $("#start_import").on("click", function(e){
                $.ajax({
                    url: "/admin/start_import",
                    type: "POST",
                    data: {
                        fileName: '{{ $fileName }}'
                    },
                    dataType: "json",
                    beforeSend(){
                        $("#loading").slideDown();
                        $("#start_import").prop('disabled', true);
                    },
                    success(response){
                        $("#loading").slideUp();
                        $("#start_import").prop('disabled', false);
                        $.notify({
                            message: response['message']
                        }, {
                            type: response['success'] ? 'success' : 'danger'
                        });
                    }
                })
            });
        });
    </script>
@endsection
@endif