@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="card-title font-weight-bold">Сообщения об ошибках</h2>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <th>#</th>
                        <th>Слово</th>
                        <th>Сообщение</th>
                        <th>Пользователь</th>
                        {{--<th>Статус</th>--}}
                        <th>Дата</th>
                    </thead>
                    @foreach($reportedErrors as $key => $error)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>
                                @if($error->translation_id)
                                <a href="/admin/translations/edit/{{ $error->translation()->id }}">{{ $error->translation()->kz }}</a>
                                @else
                                    {{ $error->selected_text }}
                                @endif
                            </td>
                            <td>{{ $error->message }}</td>
                            <td>
                                @if($error->user_id)
                                    <a href="/admin/users/{{ $error->user_id }}">{{ $error->user()->email ?? '' }}</a>
                                @else
                                    <i>Незнакомец</i>
                                @endif
                            </td>
                            {{--<td>{{ $error->status() }}</td>--}}
                            <td>{{ $error->created_at }}</td>
                        </tr>
                    @endforeach
                </table>
                {{ $reportedErrors->links() }}
            </div>
        </div>
    </div>
@endsection