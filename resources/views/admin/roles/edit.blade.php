@extends('layouts.admin')

@section('style')
    <link rel="stylesheet" href="https://unpkg.com/multiple-select@1.5.2/dist/multiple-select.min.css">
    <style>
        .ms-parent {
            width: 100% !important;
        }

        .ms-drop ul>li {
            padding: 1rem 8px;
        }
        .ms-drop ul>li:hover {
            background-color: #eee;
        }

        .ms-choice {
            height: 40px;
            line-height: 40px;
        }
    </style>
@endsection

@section('content')
    <div class="card rounded-0">
        <div class="card-header border-bottom">
            <h2 class="card-title font-weight-bold">Роль {{ $role->name }}</h2>
        </div>
        <div class="card-body pt-0">
            <div class="row">
                <div class="col-md-5">
                    <form action="{{ route('role.update', $role->id) }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input type="text" class="form-control" name="name" required value="{{ $role->name }}" @if($role->name == 'admin') disabled @endif>
                        </div>

                        <div class="form-group">
                            <label for="permissions">Права:</label>
                            <div>
                                @if($role->name == 'admin')
                                    У этого пользователя имеются все существующие права.
                                @else
                                    <select name="permissions[]" id="permissions" multiple style="min-height:470px;">
                                        @foreach($permissions as $permission)
                                            <option value="{{ $permission->id }}" {{ $role->hasPermissionTo($permission->name) ? 'selected' : '' }}>{{ $permission->title }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>

                        <a href="/admin/roles" class="btn btn-secondary btn-round btn-lg">Назад</a>
                        <button class="btn btn-success btn-round btn-lg" @if($role->name == 'admin') disabled @endif>Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://unpkg.com/multiple-select@1.5.2/dist/multiple-select.min.js"></script>
    <script>
        $('select').multipleSelect({
            displayTitle: true,
            maxHeight: 300,
            minimumCountSelected: 15,
            selectAll: false
        });
    </script>
@endsection