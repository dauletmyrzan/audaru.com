@extends('layouts.admin')

@section('content')
    <div class="card rounded-0">
        <div class="card-header border-bottom">
            <a href="/admin/users" class="btn btn-secondary btn-round btn-lg">Назад</a>
            <h2 class="card-title font-weight-bold">Роли</h2>
        </div>
        <div class="card-body pt-0">
            <table class="table table-hover">
                <thead>
                    <th style="width:100px;">#</th>
                    <th>Название</th>
                </thead>
                @foreach ($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            <a href="{{ route('role.edit', $role->id) }}" class="btn btn-success btn-sm delete-role" data-id="{{ $role->id }}">Редактировать</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection