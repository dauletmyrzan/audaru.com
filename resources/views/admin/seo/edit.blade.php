@extends('layouts.admin')

@section('style')
    <style>
        .seo-anchor {

        }

        .seo-anchor:hover {
            text-decoration: none;
            color: #4bb5ff;
        }

        label {
            color: #222!important;
            font-size: 12pt!important;
        }
         textarea {
             min-height: 300px;
             background-color: #0c2646!important;
             color: #eee!important;
         }
    </style>
@endsection

@section('content')
    <div class="card rounded-0">
        <div class="card-header border-bottom">
            <h2 class="card-title font-weight-normal">SEO-настройка страницы "{{ $seo->name }}"</h2>
        </div>
        <div class="card-body pt-0">
            <form action="{{ route('seo.update') }}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{ $seo->id }}">
                <div class="form-group py-3">
                    <label for="code_en">Код для вставки в <code>&lt;head&gt;&lt;/head&gt;</code>. Введите свои настройки SEO в виде тегов:</label>
                    <h5>Для английской версии:</h5>
                    <textarea name="code_en" class="form-control">{{ $seo->code_en }}</textarea>
                </div>
                <div class="form-group py-3">
                    <h5>Для казахской версии:</h5>
                    <textarea name="code_kz" class="form-control">{{ $seo->code_kz }}</textarea>
                </div>
                <div class="alert alert-danger alert-permanent d-inline-block">Внимание! Эти настройки перезапишут настройки по умолчанию.</div><br>
                <button class="btn btn-success btn-lg btn-round">Сохранить</button>
                <a href="/admin/seo" class="btn btn-secondary btn-lg btn-round">Назад</a>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {

        });
    </script>
@endsection