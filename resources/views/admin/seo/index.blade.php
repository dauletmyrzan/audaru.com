@extends('layouts.admin')

@section('style')
    <style>
        .seo-anchor {

        }

        .seo-anchor:hover {
            text-decoration: none;
            color: #4bb5ff;
        }
    </style>
@endsection

@section('content')
    <div class="card rounded-0">
        <div class="card-header border-bottom">
            <h2 class="card-title font-weight-bold">SEO-настройка</h2>
        </div>
        <div class="card-body pt-0">
            @foreach ($seo as $row)
                <div class="p-3">
                    <a href="{{ route('seo.edit', $row->id) }}" class="h5 seo-anchor">{{ $row->name }}</a>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
        });
    </script>
@endsection