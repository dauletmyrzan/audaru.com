@extends('layouts.admin')

@section('content')
    <div class="card rounded-0" id="translationTableCard">
        <div class="card-header">
            <ul class="nav nav-pills nav-fill mb-3">
                <li class="nav-item">
                    <a class="nav-link" href="/admin/stats">История запросов</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/admin/stats/frequent">Частые запросы</a>
                </li>
            </ul>
            <div class="row mb-3">
                <div class="col-md-3">
                    <label>Выберите период</label>
                    <input type="text" class="form-control datepicker-input" placeholder="YYYY-MM-DD" id="start_date">
                </div>
                <div class="col-md-3">
                    <label>&nbsp;</label>
                    <input type="text" class="form-control datepicker-input" placeholder="YYYY-MM-DD" id="end_date">
                </div>
                <div class="col-md-3">
                    <label>&nbsp;</label><br>
                    <button class="btn btn-primary m-0" id="load_data">Применить</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="my-3">
                <h5 class="font-weight-bold">Всего данных: {{ $stats->total() }}</h5>
            </div>
            <div class="table-responsive">
                <table class="table table-hover" id="stats_table">
                    <thead class="thead-light">
                        <th>Запрос</th>
                        <th>Количество</th>
                        <th>Статус</th>
                    </thead>
                    <tbody>
                    @foreach($stats as $key => $stat)
                        <tr>
                            <td class="query"><b>{{ $stat->query }}</b></td>
                            <td>{{ $stat->count }}</td>
                            <td><button class="btn btn-success check-translation">Проверить перевод</button></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-body">
            {{ $stats->links() }}
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            let start_date = moment(), end_date;
            $("#start_date").on("dp.change", function(){
                start_date = $(this).val();
            });
            $("#end_date").on("dp.change", function(){
                end_date = $(this).val();
            });
            $("#load_data").on("click", function(){
                if(start_date && end_date){
                    getData();
                }
            });
            var getData = function(){
                $.ajax({
                    url: '/admin/getData',
                    data: {
                        start_date: start_date,
                        end_date: end_date,
                        page: 'frequent'
                    },
                    dataType: 'json',
                    beforeSend(){
                        $("#stats_table").addClass("loading");
                    },
                    success(response){
                        $('#stats_table tbody').html(response['html']);
                        $("#stats_table").removeClass("loading");
                    }
                });
            };
        });
    </script>
@endsection