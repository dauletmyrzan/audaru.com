@extends('layouts.admin')

@section('style')
    <style>
        #translationTableCard{
            position: relative;
        }
        .loading-layer{
            display: none;
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            top: 0;
            background-color: rgba(255, 255, 255, .8);
            z-index: 100;
            background-image: url('{{ asset('img/ajax-loader.gif') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: 50px;
            opacity: .7;
        }
    </style>
@endsection

@section('content')
    <div class="card rounded-0" id="translationTableCard">
        <div class="card-header">
            <ul class="nav nav-pills nav-pills-primary nav-pills-icons justify-content-center" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#translations" role="tablist">
                        <i class="now-ui-icons objects_umbrella-13"></i> Фразы
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#topics" role="tablist">
                        <i class="now-ui-icons shopping_shop"></i> Тематики
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="translations" role="tabpanel" aria-labelledby="translations-tab">
                    <div class="my-3">
                        <h5 class="font-weight-bold">Всего данных: {{ $translations->total() }}</h5>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <th>EN</th>
                                <th>KZ</th>
                                <th>Тематика</th>
                                <th>Дата удаления</th>
                                <th></th>
                                <th></th>
                            </thead>
                            <tbody>
                            @foreach($translations as $row)
                                <tr>
                                    <td>{{ $row->en }}</td>
                                    <td>{{ $row->kz }}</td>
                                    <td><span class="text-success font-weight-bold">{{ implode(', ', $row->topics->pluck('name')->toArray()) }}</span></td>
                                    <td>{{ $row->updated_at }}</td>
                                    <td><a href="/admin/translations/edit/{{ $row->id }}" class="btn btn-success">Редактировать и восстановить</a></td>
                                    <td><div data-id="{{ $row->id }}" class="delete-translation-btn nc-icon nc-simple-remove text-danger font-weight-bold"></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        @if($translations && $translations->count() > 0)
                            {{ $translations->appends(request()->input())->links() }}
                        @endif
                    </div>
                </div>
                <div class="tab-pane fade" id="topics" role="tabpanel" aria-labelledby="topics-tab">
                    <table class="table table-hover">
                        <thead>
                            <th>#</th>
                            <th>Название</th>
                            <th>Дата удаления</th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tbody>
                        @foreach($topics as $topic)
                            <tr>
                                <td>{{ $topic->id }}</td>
                                <td>{{ $topic->name }}</td>
                                <td>{{ $topic->updated_at }}</td>
                                <td><button data-id="{{ $topic->id }}" class="btn btn-success restore-topic-btn">Восстановить</button></td>
                                <td><div data-id="{{ $topic->id }}" class="delete-topic-btn nc-icon nc-simple-remove text-danger font-weight-bold"></div></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            @if($translations->count() > 0 || $topics->count() > 0)
            <form action="{{ route('clear_basket') }}" method="post">
                @csrf
                <button class="btn btn-danger btn-round btn-lg mb-3" onclick="return confirm('Вы уверены, что хотите очистить корзину?')">Очистить корзину</button>
            </form>
            @endif
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('.delete-translation-btn').on('click', function () {
                if(confirm('Вы действительно хотите безвозвратно удалить перевод?')){
                    let thisRow = $(this).closest('tr');
                    $.ajax({
                        url: '/admin/translations/delete',
                        type: 'POST',
                        data: {
                            id: $(this).data('id'),
                            permanent: true
                        },
                        dataType: 'json',
                        success(response){
                            if(response['success']){
                                thisRow.remove();
                            }
                        }
                    });
                }
            });
            $('.restore-translation-btn').on('click', function(){
                let thisRow = $(this).closest('tr');
                $.ajax({
                    url: '/admin/translations/restore',
                    type: 'POST',
                    data: {
                        id: $(this).data('id')
                    },
                    dataType: 'json',
                    success(response){
                        thisRow.remove();
                        alert(response['message']);
                    }
                });
            });
            $('.delete-topic-btn').on('click', function () {
                if(confirm('Вы действительно хотите безвозвратно удалить тематику?')){
                    let thisRow = $(this).closest('tr');
                    $.ajax({
                        url: '/admin/topics/delete',
                        type: 'POST',
                        data: {
                            id: $(this).data('id'),
                            permanent: true
                        },
                        dataType: 'json',
                        success(response){
                            if(response['success']){
                                thisRow.remove();
                            }
                        }
                    });
                }
            });
            $('.restore-topic-btn').on('click', function(){
                let thisRow = $(this).closest('tr');
                $.ajax({
                    url: '/admin/topics/restore',
                    type: 'POST',
                    data: {
                        id: $(this).data('id')
                    },
                    dataType: 'json',
                    success(response){
                        thisRow.remove();
                        alert(response['message']);
                    }
                });
            });
        });
    </script>
@endsection