@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <form action="{{ route('topics_store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">Название на английском</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="form-group">
                            <label for="name_kz">Название на казахском</label>
                            <input type="text" class="form-control" name="name_kz" id="name_kz" required>
                        </div>
                        <a href="/admin/topics" class="btn btn-light btn-round btn-lg">Назад</a>
                        <button type="submit" class="btn btn-success btn-round btn-lg">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection