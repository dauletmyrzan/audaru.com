@extends('layouts.admin')

@section('content')
    <div class="row mb-4">
        <div class="col-md-12 text-right">
            <a href="/admin/topics/create" class="btn btn-primary btn-round btn-lg">Добавить новую тематику</a>
        </div>
    </div>
    <div class="card">
        <div class="card-body table-responsive">
            <div class="my-3">
                <h5 class="font-weight-bold">Всего данных: {{ $topics->total() }}</h5>
            </div>
            <table class="table table-hover">
                <thead>
                    <th>#</th>
                    <th>Название на англйиском</th>
                    <th>Название на казахском</th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($topics as $key => $topic)
                        <tr>
                            <td>{{ $topics->firstItem() + $key }}</td>
                            <td>{{ $topic->name }}</td>
                            <td>{{ $topic->name_kz }}</td>
                            <td><a href="/admin/topics/edit/{{ $topic->id }}" class="btn btn-warning">Редактировать</a></td>
                            <td><div data-id="{{ $topic->id }}" class="delete-topic-btn nc-icon nc-simple-remove text-danger font-weight-bold"></div></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $topics->links() }}
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('.delete-topic-btn').on('click', function () {
                if(confirm('Вы действительно хотите удалить тематику?')){
                    let thisRow = $(this).closest('tr');
                    $.ajax({
                        url: '/admin/topics/delete',
                        type: 'POST',
                        data: {
                            id: $(this).data('id')
                        },
                        dataType: 'json',
                        success(response){
                            if(response['success']){
                                thisRow.remove();
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection