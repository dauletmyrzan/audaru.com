@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="h3">Редактирование тематики</div>
                    <form action="{{ route('topic_update') }}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $topic->id }}" name="id">
                        <div class="form-group">
                            <label for="name">Название на английском</label>
                            <input type="text" id="name" name="name" value="{{ $topic->name }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="name_kz">Название на казахском</label>
                            <input type="text" id="name_kz" name="name_kz" value="{{ $topic->name_kz }}" class="form-control" required>
                        </div>
                        <a href="/admin/topics" class="btn btn-light">Назад</a>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection