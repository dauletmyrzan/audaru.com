@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="card-title font-weight-normal">Экспорт словаря</h2>
        </div>
        <div class="card-body p-5">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-4">
                    <form action="{{ route('topic.export') }}">
                        <label for="topic" class="text-dark h6">Выберите тему</label>
                        <select name="topic" id="topic" class="form-control form-control-lg mt-3">
                            <option value="all">Все темы</option>
                            @foreach ($topics as $topic)
                                <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                            @endforeach
                        </select>

                        <button class="btn btn-success btn-round btn-lg mt-4">Скачать словарь</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection