@extends('layouts.app')

@section('content')
    <div class="container mt-100 min-height-85vh" id="training">
        <h1 class="font-weight-bold mt-0">{{ __('main.training') }}</h1>
        <div class="row">
            <div class="col-md-6">
                <div class="card border rounded-0 shadow-ui">
                    <div class="training-counter">1/30</div>
                    @if(\Auth::check() && \Auth::user()->hasAnyRole(['admin', 'editor']))
                    <div class="edit-translated-word text-info">
                        <i class="fas fa-pen"></i>
                    </div>
                    @endif
                    <div class="report-an-error">
                        <i class="fas fa-exclamation-triangle text-warning"></i>
                    </div>
                    <div class="card-body text-center">
                        <div class="h3 card-kz-word mt-5 mb-0"></div>
                        <div class="card-qaz-word"></div>
                        <hr>
                        <div class="h3 card-en-word mt-2"></div>
                    </div>
                    <div class="card-footer border-top pb-3">
                        <button class="btn btn-secondary btn-sm my-4 ml-4 prev-word" disabled>{{ __('main.prev_btn') }}</button>
                        <button class="btn btn-info btn-sm my-4 mr-4 next-word float-right">{{ __('main.next_btn') }}</button>
                        <div class="text-danger text-center report-error-description">@lang('main.report_error_description')</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            let lang = $("html").attr('lang');
            let words, i = 0;
            let loadWords = function(){
                $.ajax({
                    url: '/getCardWords',
                    dataType: 'json',
                    beforeSend(){
                        $(".card-body").addClass('loading');
                    },
                    success(response){
                        $(".card-body").removeClass('loading');
                        words = response['data'];
                        changeWord(i);
                    }
                });
            };
            let changeWord = function(i){
                $('.report-an-error').attr('data-id', words[i]['id']);
                $('.card').attr('data-id', words[i]['id']);
                $('.card-kz-word').text(words[i]['kz']);
                $('.card-qaz-word').text("[" + words[i]['qaz'] + "]");
                $('.card-en-word').text(words[i]['en']);
            };
            $(".prev-word").on('click', function(){
                $('.next-word').prop('disabled', false).removeClass('btn-secondary').addClass('btn-info');
                if(i > 0){
                    i--;
                    if(i === 0){
                        $('.prev-word').prop('disabled', true).removeClass('btn-info').addClass('btn-secondary');
                    }
                    changeWord(i);
                    $('.training-counter').text((i+1) + '/30');
                }
            });
            $(".next-word").on('click', function(){
                $('.prev-word').prop('disabled', false).removeClass('btn-secondary').addClass('btn-info');
                if(i < words.length-1){
                    i++;
                    changeWord(i);
                    $('.training-counter').text((i+1) + '/30');
                    if (i == 29) {
                        $('.next-word').text(lang == 'en' ? 'New' : 'Жаңа');
                    }
                } else {
                    i = 0;
                    $('.prev-word').prop('disabled', true).removeClass('btn-info').addClass('btn-secondary');
                    $('.next-word').removeClass('new-words').text(lang == 'en' ? 'Next' : 'Келесі');
                    $('.training-counter').text('1/30');
                    loadWords();
                }
            });
            $(window).on('keydown', function(e){
                if(e.which === 37){
                    $(".prev-word").trigger('click');
                }else if(e.which === 39){
                    $(".next-word").trigger('click');
                }
            });
            loadWords();
        });
    </script>
@endsection