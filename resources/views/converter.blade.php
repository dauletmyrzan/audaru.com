@extends('layouts.app')

@section('style')
    <style>
        textarea{
            min-height: 150px;
            font-size: 11pt!important;
        }
        textarea[readonly]{
            background-color: #fff!important;
            cursor: initial!important;
            color: #222!important;
        }
    </style>
@endsection

@section('content')
    <div class="container mt-100 min-height-75vh">
        <h1 class="font-weight-bold mb-3">Конвертер</h1>
        <div class="mb-3 mb-md-5">Конвертация текста языке с кириллицы на латиницу осуществляется согласно правилам «<a href="http://adilet.zan.kz/rus/docs/U1800000637" class="underline" target="_blank">Указа</a>».</div>
        <div class="row mb-3 mb-md-5">
            <div class="col-md-6 mb-4">
                <label for="text" class="font-weight-bold text-muted">Текст на кириллице:</label>
                <textarea id="text" cols="30" class="form-control form-control-white p-3 border shadow-sm" placeholder="Введите фразу..."></textarea>
            </div>
            <div class="col-md-6 mb-4">
                <label for="text_converted" class="font-weight-bold text-muted">Конвертированный текст:</label>
                <textarea id="text_converted" readonly class="form-control form-control-white p-3 border shadow-sm"></textarea>
            </div>
        </div>
        <button class="btn btn-round btn-lg font-weight-bold mx-auto d-block mb-5" style="background-color:{{$settings->color_scheme}};" id="convert">Конвертировать</button>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#text").on("keyup", function(e){
                if(e.ctrlKey && e.keyCode === 13){
                    $("#convert").trigger("click");
                }
            });
            $("#convert").on("click", function(){
                let text = $("#text").val();
                if(text.length > 0){
                    $.ajax({
                        url: '/convert',
                        data: {
                            text: text
                        },
                        beforeSend(){
                            $("#convert").prop('disabled', true);
                        },
                        success(response){
                            $("#convert").prop('disabled', false);
                            $("#text_converted").val(response['converted']);
                        }
                    });
                }
            });
            $("#text_converted").on("click", function(){
                $(this).select();
            });
        });
    </script>
@endsection