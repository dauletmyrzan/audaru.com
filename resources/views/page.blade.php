@extends('layouts.app')

@section('content')
    {!! \Session::get('locale') == 'kz' ? $page->content_kz : $page->content_en !!}
@endsection