@extends('layouts.app', [
    'seo' => $seo,
])

@section('content')
    <div class="container min-height-75vh mt-100">
        <h1 class="font-weight-bold">{{ __('main.add_translation') }}</h1>
        <div class="row">
            <div class="col-md-5">
                <form action="/translation/add" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="kz">{{ __('main.kz_phrase') }}: *</label>
                        <input type="text" class="form-control py-3 form-control-white rounded-0" name="kz" id="kz" required>
                    </div>
                    <div class="form-group">
                        <label for="en">{{ __('main.en_phrase') }}: *</label>
                        <input type="text" class="form-control py-3 form-control-white rounded-0" name="en" id="en" required>
                    </div>
                    <div class="form-group">
                        <label for="qaz">{{ __('main.qaz_phrase') }}:</label>
                        <input type="text" class="form-control py-3 form-control-white rounded-0 bg-white" name="qaz" id="qaz" required readonly>
                    </div>
                    <div class="text-md-left text-center">
                        <button type="submit" class="btn btn-round btn-lg" style="background-color:{{$settings->color_scheme}};">{{ __('main.send_moderation') }}</button>
                        <div class="text-muted mb-5">* - {{ __('main.required_fields') }}</div>
                    </div>
                </form>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">{!! Session::get('message') !!}</div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("form").on("submit", function(){
                $("button[type=submit]").prop("disabled", true);
            });
            $("#kz").on("keyup", function(){
                let word = $(this).val();
                $.ajax({
                    url: '/transliterateAjax',
                    data: {
                        word: word
                    },
                    dataType: 'json',
                    success(response){
                        if(response['success']){
                            $("#qaz").val(response['transliterated']);
                        }
                    }
                });
            });
        });
    </script>
@endsection