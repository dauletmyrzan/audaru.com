@extends('layouts.app', ['bodyClass' => 'login-page sidebar-collapse'])

@section('content')
    <div class="page-header clear-filter">
        <div class="content">
            <div class="container">
                <div class="col-md-4 ml-auto mr-auto">
                    <div class="card card-login" data-background-color="{{ $settings->color_scheme_name }}">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="card-header text-center">
                                <div class="logo-container mt-4 mb-2">
                                    <img src="{{ asset('img/logo-white.svg') }}" alt="Лого">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="h4 mt-0">{{ __('auth.reset_title') }}</div>
                                <div class="form-group no-border input-lg">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('auth.email_placeholder') }}">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="card-footer text-center mt-0 px-3">
                                <button class="btn btn-neutral btn-round btn-lg w-100 font-weight-bold mb-5">{{ __('auth.reset_btn') }}</button><br>
                                <div class="pull-left">
                                    <h6><a class="link" href="/login">{{ __('main.sign_in') }}</a></h6>
                                </div>
                                <div class="pull-right">
                                    <h6><a href="/register" class="link">{{ __('auth.sign_up') }}</a></h6>
                                </div>
                            </div>
                        </form>
                        @if($errors->any())
                                <div class="alert alert- alert-permanent">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection