@extends('layouts.app', ['bodyClass' => 'login-page sidebar-collapse'])

@section('content')
    <div class="page-header clear-filter">
        <div class="page-header-image"></div>
        <div class="content">
            <div class="container">
                <div class="col-md-5 ml-auto mr-auto">
                    <div class="card card-login" data-background-color="{{ $settings->color_scheme_name }}">
                        <form class="form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="card-header text-center">
                                <div class="logo-container mt-5">
                                    <img src="{{ asset('img/logo.png') }}" alt="Лого">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="h3 mt-0">{{ __('auth.login_title') }}</div>
                                <div class="form-group no-border input-lg">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('auth.email_placeholder') }}">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group no-border input-lg">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('auth.password_placeholder') }}">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="remember" style="vertical-align:middle;" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="h6 m-0" for="remember" style="vertical-align:middle;">{{ __('auth.remember_me') }}</label>
                                </div>
                                <div id="recaptcha"></div>
                                @error('g-recaptcha-response')
                                    <span class="invalid-feedback" style="display:block;" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="card-footer text-center mt-0 px-3">
                                <button class="btn btn-neutral btn-round btn-lg font-weight-bold w-100 mt-0 mb-3">{{ __('auth.sign_in_btn') }}</button><br>
                                <hr>
                                <div class="socialite mb-5">
                                    <div style="font-size:10pt;">@lang('auth.social')</div>
                                    <a href="{{ url('/auth/redirect/google') }}" class="btn btn-neutral btn-round btn-icon" title="Google"><i class="fab fa-google"></i></a>
                                </div>
                                <div class="pull-left">
                                    <h6>
                                        <a href="/register" class="link">{{ __('auth.sign_up') }}</a>
                                    </h6>
                                </div>
                                <div class="pull-right">
                                    @if (Route::has('password.request'))
                                        <h6><a class="link" href="{{ route('password.request') }}">{{ __('auth.forgot_password') }}</a></h6>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!!  GoogleReCaptchaV2::render('recaptcha') !!}
@endsection
