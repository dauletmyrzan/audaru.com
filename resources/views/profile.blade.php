@extends('layouts.app')

@section('content')
    <div class="container mt-100 min-height-85vh">
        <h1 class="font-weight-bold">{{ __('main.profile') }}</h1>
        <div class="row">
            <div class="col-md-6">
                <div class="card border shadow-sm">
                    <div class="card-body">
                        <form action="{{ route('profile.update') }}" method="post">
                            @csrf
                            <div class="form-group mb-4">
                                <label for="name">@lang('form.name'): *</label>
                                <input type="text" name="name" class="form-control" id="name" required value="{{ $user->name }}">
                            </div>
                            <div class="form-group mb-3">
                                <label for="surname">@lang('form.surname'): </label>
                                <input type="text" name="surname" class="form-control" id="surname" required value="{{ $user->surname }}">
                            </div>
                            <div class="form-group mb-3">
                                <label for="email">Email: </label>
                                <input type="text" name="email" class="form-control" id="email" readonly required value="{{ $user->email }}">
                            </div>
                            <button type="submit" class="btn" style="background-color: {{ $settings->color_scheme }};">{{ __('form.submit_btn') }}</button>
                        </form>
                    </div>
                    @if($errors->any())
                        <div class="alert mb-0 alert-danger alert-permanent">
                            <ul class="mb-0">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-success mb-0">{{ Session::get('message') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="card border shadow-sm">
                    <div class="card-body text-center" style="height:352px;">
                        <div class="h3 font-weight-bold mt-5 mb-3">@lang('training.title')</div>
                        <div class="text-muted mb-2">@lang('training.description')</div>
                        <a class="btn btn-primary btn-lg mt-5" href="/cards">@lang('training.start_btn')</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header p-3">
                        <h2 class="h3 mb-0">@lang('main.my_translations')</h2>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>#</th>
                                    <th>@lang('main.kazakh')</th>
                                    <th>@lang('main.english')</th>
                                    <th>@lang('main.qazaq')</th>
                                    <th>@lang('main.created_at')</th>
                                    <th>@lang('main.status')</th>
                                </thead>
                                <tbody>
                                    @foreach($user->translations as $key => $translation)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $translation->kz }}</td>
                                            <td>{{ $translation->en }}</td>
                                            <td>{{ $translation->qaz }}</td>
                                            <td>{{ $translation->created_at }}</td>
                                            <td>{!! $translation->status() !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection