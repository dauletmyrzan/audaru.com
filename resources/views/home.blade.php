@extends('layouts.app', [
    'seo' => $seo,
])

@section('style')
    <link rel="stylesheet" href="{{ asset('css/jqkeyboard.css') }}">
@endsection

@section('content')
    <div class="container mt-100 py-4 min-height-90vh">
        <h1 class="font-weight-bold text-center text-md-left" id="main_h1">{{ __('main.main_h1') }}</h1>
        <div class="row">
            <div class="col-md-8">
                <div class="row mb-3 keyboard-holder" @if(isset($_COOKIE['keyboard']) && $_COOKIE['keyboard'] == 'hidden') style='display:none;' @endif>
                    <div class="col-md-12">
                        <div id="keyboard"></div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center mb-3" id="searchPanel">
                    <div class="col-md-5">
                        <select name="lang_from" id="lang_from" class="form-control form-control-white rounded-0 py-3">
                            <option value="kz" {{ \App::getLocale() == 'kz' ? 'selected' : '' }}>{{ __('main.kazakh') }}</option>
                            <option value="qaz">Qazaqsha</option>
                            <option value="en" {{ \App::getLocale() == 'en' ? 'selected' : '' }}>English</option>
                        </select>
                    </div>
                    <div class="col-md-2 text-center">
                        <button class="btn btn-white switch-lang"><i class="fas fa-exchange-alt"></i></button>
                    </div>
                    <div class="col-md-5">
                        <select name="lang_to" id="lang_to" class="form-control form-control-white rounded-0 py-3">
                            <option value="kz" {{ \App::getLocale() == 'en' ? 'selected' : '' }}>{{ __('main.kazakh') }}</option>
                            <option value="qaz">Qazaqsha</option>
                            <option value="en" {{ \App::getLocale() == 'kz' ? 'selected' : '' }}>English</option>
                        </select>
                    </div>
                </div>
                <div class="row" id="search_block">
                    <div class="col-md-8 mb-4">
                        <div class="input-group">
                            <input type="text" placeholder="{{ __('main.search_input_placeholder') }}" class="form-control form-control-white form-control-lg py-3 rounded-0 search-input">
                            <div class="input-group-append">
                                <span class="input-group-text rounded-0 px-2 show-history" @if(!\Session::has('history')) style="display: none;" @endif><i class="fas fa-history"></i></span>
                                <span class="input-group-text rounded-0 px-2 border-left-0 clear-input" title="{{ __('main.clear') }}"><i class="fas fa-times-circle"></i></span>
                                <span class="input-group-text rounded-0 pl-2 pr-3 keyboard-toggle" title="@if(isset($_COOKIE['keyboard']) && $_COOKIE['keyboard'] == 'visible') {{ __('main.hide_keyboard') }} @else {{ __('main.show_keyboard') }} @endif"><i class="fas fa-keyboard"></i></span>
                            </div>
                        </div>
                        <div class="my-2 random-phrases">
                            <div class="d-inline-block">{{ __('main.example') }}: </div>
                            <span class="random-phrases-kz" {{ \App::getLocale() == 'kz' ? 'style=display:inline-block' : '' }}>{!! implode(', ', $randomPhrases['kz']) !!}</span>
                            <span class="random-phrases-qaz">{!! implode(', ', $randomPhrases['qaz']) !!}</span>
                            <span class="random-phrases-en" {{ \App::getLocale() == 'en' ? 'style=display:inline-block' : '' }}>{!! implode(', ', $randomPhrases['en']) !!}</span>
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">
                        <button class="btn py-3 m-0 btn-round w-100 translate-btn" id="translate_btn" style="background-color:{{ $settings->color_scheme }}">{{ __('main.translate') }}</button>
                    </div>
                </div>
                <div class="row result-row">
                    <div class="col-md-12">
                        <a href="/translation/add" class="link mb-2 float-right add-translation">{{ __('main.add_translation') }}</a>
                        <div class="card result-card">
                            <div class="card-body" id="result"></div>
                            <div class="card-footer border-top p-3">
                                <div class="text-danger report-error-description">@lang('main.report_error_description')</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <a href="http://translators-group.kz/ru/" target="_blank">
                    <div class="banner-right p-3" style="background-image: url({{ asset('img/banner.png') }})"></div>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/jqk.layout.lang.js?v=3.0') }}"></script>
    <script src="{{ asset('js/jqkeyboard-min.js?v=3.0') }}"></script>
    <script src="{{ asset('js/app.js?v=3.2') }}"></script>
    @if(!Agent::isMobile())
        <script>
            $(window).on('scroll', function(){
                let top = $("#searchPanel").offset().top;
                let scrollTop = $(this).scrollTop();
                if(scrollTop > top-70 && $("#result:not(:empty)").length){
                    $("#navbarSearchPanel").show();
                }else{
                    $("#navbarSearchPanel").hide();
                }
                if(scrollTop > 400){
                    $("#scrollUp").show();
                }else{
                    $("#scrollUp").hide();
                }
            });
        </script>
    @endif
@endsection