@extends('layouts.app')

@section('content')
    <div class="container mt-100 min-height-85vh">
        <div class="section my-5 shadow-ui px-2">
            <div class="row justify-content-md-center">
                <div class="text-center col-md-12 col-lg-8">
                    <h1 class="title">Что такое Audaru.com?</h1>
                    <div class="description">
                        <p>Audaru - это первый в Казахстане англо-казахский онлайн словарь. База словаря данного проекта составляет ~900 000 переводов.</p>
                        <p>Проект обновляется и оптимизируется каждый день.</p>
                        <p>Вы можете внести свою лепту, добавив слова, которых нет в словаре, а также предложить исправления к текущим фразам.</p>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row justify-content-md-center sharing-area text-center">
                <div class="text-center col-md-12 col-lg-8">
                    <h5 class="font-weight-bold">Поделитесь проектом в социальных сетях!</h5>
                </div>
                <div class="text-center col-md-12 col-lg-8">
                    <a target="_blank" href="https://www.twitter.com" class="btn btn-neutral shadow-sm btn-icon btn-twitter btn-round btn-lg" rel="tooltip" title="" data-original-title="Follow us">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a target="_blank" href="https://www.facebook.com" class="btn btn-neutral shadow-sm btn-icon btn-facebook btn-round btn-lg" rel="tooltip" title="" data-original-title="Like us">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                    <a target="_blank" href="https://www.instagram.com" class="btn btn-neutral shadow-sm btn-icon btn-instagram btn-lg btn-round" rel="tooltip" title="" data-original-title="Follow us">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>

        </div>
    </div>
@endsection