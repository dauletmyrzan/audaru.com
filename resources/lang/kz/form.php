<?php

return [
    'name' => 'Аты',
    'surname' => 'Тегі',
    'email' => 'Email',
    'password' => 'Құпиясөз',
    'submit_btn' => 'Жіберу',
    'save_btn' => 'Сақтау',
    'close' => 'Жабу'
];