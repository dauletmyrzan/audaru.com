<?php

return [
    'title' => 'Білім карталары',
    'description' => 'Сөздік қорыңызды толықтыру үшін карталар жиынтығын жаттаңыз',
    'start_btn' => 'Бастау'
];