<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'               => 'These credentials do not match our records.',
    'throttle'             => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_title'          => 'Sign in',
    'email_placeholder'    => 'Enter email',
    'password_placeholder' => 'Enter password',
    'remember_me'          => 'Remember me',
    'sign_in_btn'          => 'Sign in',
    'sign_up'              => 'Sign up',
    'forgot_password'      => 'Forgot password',
    'name_placeholder'     => 'Enter name',
    'password_re'          => 'Repeat password',

    'reset_title' => 'Reset password',
    'reset_btn'   => 'Reset',
    'social'      => 'Sign in using social networks',
    'recaptcha'   => 'Please confirm that you are not a robot.',
];
