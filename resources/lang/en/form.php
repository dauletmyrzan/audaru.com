<?php

return [
    'name' => 'Name',
    'surname' => 'Surname',
    'email' => 'Email',
    'password' => 'Password',
    'submit_btn' => 'Submit',
    'save_btn' => 'Save',
    'close' => 'Close'
];