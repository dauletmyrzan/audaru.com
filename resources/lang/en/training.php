<?php

return [
    'title' => 'Training cards',
    'description' => 'Explore card sets to complement your vocabulary',
    'start_btn' => 'Start'
];