<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('admin')->middleware(['auth', 'role:admin|editor|seo'])->group(function(){
    /**
     * ----------------------------------
     * Pages
     * ----------------------------------
    */
    Route::get('/', [
        \App\Http\Controllers\AdminController::class,
        'index'
    ])->name('admin');
    Route::get('/user-phrases', 'TranslationController@indexUserPhrases')->middleware('permission:user-phrases');
    Route::get('/topics', [
        'uses' => 'TopicController@index',
        'as' => 'topics'
    ])->middleware('permission:read topics|edit topics');
    Route::get('/settings', [
        'uses' => 'SettingsController@index',
        'as' => 'settings'
    ])->middleware('permission:read settings|edit settings');
    Route::get('/profile', [
        'uses' => 'AdminController@profile',
        'as' => 'admin.profile'
    ]);
    Route::get('/stats', [
        'uses' => 'AdminController@stats',
        'as' => 'stats'
    ])->middleware('permission:read statistics');
    Route::get('/stats/frequent', [
        'uses' => 'AdminController@statsFrequent'
    ])->middleware('permission:read statistics');
    Route::get('/translation/import', [
        'uses' => 'TranslationController@showImport',
    ])->middleware('permission:upload dictionary');


    Route::post('/stats/clear', [
        \App\Http\Controllers\AdminController::class,
        'clearStats'
    ])->middleware('permission:read statistics')->name('stats.clear');

    /**
     * ----------------------------------
     * Translations
     * ----------------------------------
    */
    Route::get('/translation', [
        \App\Http\Controllers\TranslationController::class,
        'index',
    ])->name('translations')->middleware('permission:read dictionary|edit dictionary');
    Route::get('/translation/create', [
        'uses' => 'TranslationController@create'
    ])->middleware('permission:edit dictionary');
    Route::post('/translation/store', [
        'uses' => 'TranslationController@store',
        'as' => 'translation_store'
    ])->middleware('permission:edit dictionary');
    Route::post('/translations/delete', [
        'uses' => 'TranslationController@destroy',
        'as' => 'translation_delete'
    ])->middleware('permission:edit dictionary');
    Route::get('/translations/edit/{id}', [
        'uses' => 'TranslationController@edit'
    ])->middleware('permission:edit dictionary');
    Route::post('/translations/update', [
        'uses' => 'TranslationController@update',
        'as' => 'translation_update'
    ])->middleware('permission:edit dictionary');
    Route::post('/translations/restore', [
        'uses' => 'TranslationController@restore',
        'as' => 'translation_restore'
    ])->middleware('permission:edit dictionary');
    Route::post('/translations/approve', [
        'uses' => 'TranslationController@approve'
    ])->middleware('permission:edit dictionary');
    Route::post('/translations/decline', [
        'uses' => 'TranslationController@decline'
    ])->middleware('permission:edit dictionary');
    Route::get('/search', [
        \App\Http\Controllers\TranslationController::class,
        'search'
    ])->name('admin.search');

    /**
     * ----------------------------------
     * Topics
     * ----------------------------------
    */
    Route::get('/topics/create', [
        'uses' => 'TopicController@create'
    ])->middleware('permission:edit topics');
    Route::post('/topics/store', [
        'uses' => 'TopicController@store',
        'as' => 'topics_store'
    ])->middleware('permission:edit topics');
    Route::get('/topics/edit/{id}', [
        'uses' => 'TopicController@edit'
    ])->middleware('permission:edit topics');
    Route::post('/topics/update', [
        'uses' => 'TopicController@update',
        'as' => 'topic_update'
    ])->middleware('permission:edit topics');
    Route::post('/topics/delete', [
        'uses' => 'TopicController@destroy'
    ])->middleware('permission:edit topics');
    Route::post('/topics/restore', [
        'uses' => 'TopicController@restore'
    ])->middleware('permission:edit topics');
    Route::post('/topics/delete-with-translations', [
        \App\Http\Controllers\TopicController::class,
        'deleteWithTranslations',
    ])->middleware('permission:edit topics');
    Route::get('/topics/export', [
        \App\Http\Controllers\TopicController::class,
        'exportIndex',
    ])->middleware('permission:download dictionary');
    Route::get('/topic-export', [
        \App\Http\Controllers\TopicController::class,
        'export'
    ])->name('topic.export')->middleware('permission:download dictionary');

    /**
     * ----------------------------------
     * For statistics
     * ----------------------------------
    */
    Route::get('/getData', [
        'uses' => 'AdminController@getData',
        'middleware' => ['ajax', 'cors']
    ])->middleware('permission:read statistics');
    Route::get('/checkTranslation', [
        'uses' => 'TranslationController@checkTranslation',
        'middleware' => ['ajax', 'cors']
    ]);
    Route::post('/upload_base', [
        'uses' => 'TranslationController@uploadBase',
        'as' => 'upload_base'
    ]);
    Route::post('/start_import', [
        'uses' => 'TranslationController@startImport',
        'as' => 'start_base',
        'middleware' => ['ajax', 'cors']
    ]);

    /**
     * ----------------------------------
     * Basket
     * ----------------------------------
    */
    Route::get('/basket', [
        'uses' => 'AdminController@basket',
        'as' => 'basket'
    ])->middleware('permission:basket');
    Route::post('/clear_basket', [
        'uses' => 'AdminController@clearBasket',
        'as' => 'clear_basket'
    ])->middleware('permission:basket');
    Route::get('/ajax/search_translation', [
        'uses' => 'TranslationController@search',
        'middleware' => ['ajax', 'cors']
    ]);
    Route::get('/getEditForm', [
        'uses' => 'AdminController@getEditForm',
        'as' => 'get_edit_form'
    ]);

    Route::post('/settings/update', [
        'uses' => 'SettingsController@update'
    ]);

    /**
     * ----------------------------
     * Reported errors
     * ----------------------------
    */
    Route::get('/reported-errors', [
        'uses' => 'ReportedErrorController@index'
    ])->middleware('permission:reported-errors');

    /**
     * ----------------------------
     * Pages
     * ----------------------------
    */
    Route::get('/pages', 'PageController@index')->middleware('permission:pages');
    Route::get('/pages/about', 'PageController@edit')->middleware('permission:pages');
    Route::get('/pages/contacts', 'PageController@edit')->middleware('permission:pages');
    Route::get('/pages/footer', 'PageController@edit')->middleware('permission:pages');
    Route::post('/pages/update', [
        'uses' => 'PageController@update'
    ])->middleware('permission:pages');

    /**
     * ----------------------------
     * Users
     * ----------------------------
    */
    Route::get('/users', [
        'uses' => 'UserController@index',
        'as' => 'users'
    ])->middleware('permission:read users|edit users');
    Route::get('/users/{id}', 'UserController@show')->middleware('permission:read users|edit users');
    Route::post('/users/update', [
        'uses' => 'UserController@update',
        'as' => 'user.update'
    ])->middleware('permission:edit users');
    Route::post('/users/delete', [
        \App\Http\Controllers\UserController::class,
        'delete',
    ])->middleware('permission:edit users');
    Route::post('/deleteUser', 'UserController@deleteUser')->middleware('permission:edit users');

    /**
     * ----------------------------
     * Journal
     * ----------------------------
     */
    Route::get('/journal/', [
        'uses' => 'TranslationEventController@index',
        'as' => 'journal.index'
    ])->middleware('permission:journal');

    Route::post('/journal/clear', [
        \App\Http\Controllers\TranslationEventController::class,
        'clear'
    ])->middleware('permission:journal')->name('journal.clear');

    /**
     * ----------------------------
     * Роли
     * ----------------------------
     */
    Route::get('/roles', [
        \App\Http\Controllers\AdminController::class,
        'roles',
    ])->name('roles')->middleware('permission:edit roles');

    Route::get('/roles/{id}/edit', [
        \App\Http\Controllers\AdminController::class,
        'editRole',
    ])->name('role.edit')->middleware('permission:edit roles');

    Route::post('/roles/{id}/update', [
        \App\Http\Controllers\AdminController::class,
        'updateRole',
    ])->name('role.update')->middleware('permission:edit roles');

    /**
     * ----------------------------
     * Права
     * ----------------------------
     */
    Route::get('/permissions', [
        \App\Http\Controllers\AdminController::class,
        'permissions',
    ])->name('permissions');

    /**
     * -------------------------------
     * Дубликаты
     * -------------------------------
     */
    Route::prefix('/duplicates')->middleware('permission:duplicates')->group(function () {
        Route::get('/', [
            \App\Http\Controllers\DuplicateController::class,
            'index',
        ]);
    });

    Route::prefix('/seo')->middleware('permission:read seo|edit seo')->group(function () {
        Route::get('/', [
            \App\Http\Controllers\SeoController::class,
            'index',
        ])->name('seo');
        Route::get('/edit/{id}', [
            \App\Http\Controllers\SeoController::class,
            'edit',
        ])->name('seo.edit');
        Route::post('/update', [
            \App\Http\Controllers\SeoController::class,
            'update'
        ])->name('seo.update');
    });
});

/**
 * -----------------------------------
 * AJAX
 * -----------------------------------
*/
Route::get('/getRequestsHistory', [
    'uses' => 'TranslationController@getRequestsHistory',
    'middleware' => ['ajax', 'cors']
]);
Route::get('/transliterateAjax', [
    'uses' => 'TranslationController@transliterateAjax',
    'middleware' => ['ajax', 'cors']
]);
Route::get('/getCardWords', [
    'uses' => 'HomeController@getCardWords',
    'middleware' => ['ajax', 'cors']
]);

/**
 * -----------------------------------
 * Users
 * -----------------------------------
*/
Route::get('/profile', [
    'uses' => 'UserController@show',
    'as' => 'profile'
]);
Route::post('/profile.update', [
    'uses' => 'UserController@updateProfile',
    'as' => 'profile.update'
]);
Route::post('/report_error', [
    'uses' => 'ReportedErrorController@store',
    'as' => 'report_error',
    'middleware' => ['ajax', 'cors']
]);

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

/**
 * -----------------------------------
 * Adding translation by user
 * -----------------------------------
*/
Route::get('/translation/add', [
    'uses' => 'TranslationController@addIndex',
    'middleware' => 'auth'
]);

Route::post('/translation/add', [
    'uses' => 'TranslationController@add'
]);

/**
 * -----------------------------------
 * About page
 * -----------------------------------
 */
Route::get('/about', [
    'uses' => 'PageController@show'
]);
/**
 * -----------------------------------
 * Contacts page
 * -----------------------------------
 */
Route::get('/contacts', [
    'uses' => 'PageController@show'
]);

/**
 * -----------------------------------
 * Cards
 * -----------------------------------
 */
Route::get('/cards', [
    'uses' => 'HomeController@cards'
]);

/**
 * -----------------------------------
 * Converter
 * -----------------------------------
 */
Route::get('/converter', [
    'uses' => 'HomeController@converter'
]);
Route::get('/convert', [
    'uses' => 'HomeController@convert',
    'middleware' => ['ajax', 'cors']
]);

/**
 * -----------------------------------
 * Local API
 * -----------------------------------
*/
Route::get('/api/translate', [
    'uses' => 'SearchController@search',
    'as' => 'translate_api',
    'middleware' => ['ajax', 'cors']
]);
Route::get('/api/translate/autocomplete', [
    'uses' => 'SearchController@searchAutocomplete',
    'as' => 'translate_api_autocomplete',
    'middleware' => ['ajax', 'cors']
]);
Route::get('/getRandomPhrases', [
    'uses' => 'HomeController@getRandomPhrases',
    'middleware' => ['ajax', 'cors']
]);

Route::get('/lang/{locale}', [
    \App\Http\Controllers\HomeController::class,
    'setLocale'
]);

Route::get('/policy', [
    \App\Http\Controllers\HomeController::class,
    'policy'
]);

Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');
Route::get('/authById/{id}', [
    \App\Http\Controllers\UserController::class,
    'authById',
]);