-- проставляет тематику "общая" всем переводам без тем
insert into topic_translation (`translation_id`, `topic_id`) select `id`, 6 from translations where translations.id not in (select translation_id from topic_translation)

-- достает topic из таблицы translations, находит его id и вставляет в таблицу topic_translation
insert into topic_translation (translation_id, topic_id) select t1.id as translation_id, t2.id as topic_id from translations t1
inner join topics t2 on t2.name = t1.topic
where t1.topic is not null
and not exists
(select translation_id from topic_translation tt where tt.translation_id = t1.id and tt.topic_id = t2.id);

-- удаляет дубли
delete from translations where id in ( select * from (select min(t.id) from translations t group by t.en, t.kz having count(t.en) > 1 and count(t.kz) > 1) x )