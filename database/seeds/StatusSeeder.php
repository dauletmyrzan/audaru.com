<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            ['name' => 'Активно'],
            ['name' => 'На модерации'],
            ['name' => 'Отклонено'],
            ['name' => 'В корзине'],
            ['name' => 'Неактивно']
        ];
        DB::table('translation_statuses')->truncate();
        DB::table('translation_statuses')->insert($arr);

        $errorStatuses = [
            ['name' => 'Отработано'],
            ['name' => 'На модерации'],
            ['name' => 'Отклонено']
        ];
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('error_statuses')->truncate();
        DB::table('error_statuses')->insert($errorStatuses);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
