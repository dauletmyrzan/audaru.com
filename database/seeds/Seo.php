<?php

use Illuminate\Database\Seeder;

class Seo extends Seeder
{
    protected $defaultSeo = [
        [
            'name' => 'Главная',
            'code' => '',
            'path' => '/',
        ],
        [
            'name' => 'Добавление нового слова',
            'code' => '',
            'path' => '/translation/add',
        ],
        [
            'name' => 'Тренинг',
            'code' => '',
            'path' => '/cards',
        ],
        [
            'name' => 'О компании',
            'code' => '',
            'path' => '/about',
        ],
        [
            'name' => 'Контакты',
            'code' => '',
            'path' => '/contacts',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->defaultSeo as $setting) {
            $seo       = new \App\Seo();
            $seo->name = $setting['name'];
            $seo->code = $setting['code'];
            $seo->path = $setting['path'];
            $seo->save();
        }
    }
}
