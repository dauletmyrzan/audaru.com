<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSettingsAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('title');
            $table->string('keywords');
            $table->string('description');
            $table->string('image_src')->default('https://audaru.com/img/social.png');
            $table->string('url')->default('https://audaru.com');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('keywords');
            $table->dropColumn('description');
            $table->dropColumn('image_src');
            $table->dropColumn('url');
        });
    }
}
