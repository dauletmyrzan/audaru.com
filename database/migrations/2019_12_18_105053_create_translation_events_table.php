<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslationEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id');
            $table->string('en');
            $table->string('kz');
            $table->string('qaz');
            $table->integer('rating');
            $table->integer('status_id');
            $table->unsignedBigInteger('translation_id');
            $table->boolean('deleted')->default(0);
            $table->foreign('author_id')->references('id')->on('users');
            $table->foreign('translation_id')->references('id')->on('translations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation_events');
    }
}
