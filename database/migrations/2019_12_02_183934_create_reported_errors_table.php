<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportedErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reported_errors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('message');
            $table->unsignedBigInteger('translation_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('error_status_id');
            $table->timestamps();
            $table->foreign('translation_id')->references('id')->on('translations');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('error_status_id')->references('id')->on('error_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reported_errors');
    }
}
