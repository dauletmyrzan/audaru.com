<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seo';

    public function getCodeEnAttribute($code)
    {
        return base64_decode($code);
    }

    public function getCodeKzAttribute($code)
    {
        return base64_decode($code);
    }
}
