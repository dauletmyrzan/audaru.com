<?php
namespace App\Exports;

use App\Translation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TranslationsExport implements FromQuery, WithHeadings, ShouldQueue, WithMapping
{
    use Exportable;

    /**
     * Коллекция переводов.
     *
     * @var \App\Topic|null
     */
    private $topic;

    /**
     * Конструктор.
     *
     * @param $topic
     */
    public function __construct($topic)
    {
        $this->topic = $topic;
    }

    public function headings(): array
    {
        return [
            'EN', 'KZ', 'TOPIC',
        ];
    }

    public function query()
    {
        if ($this->topic === null) {
            return Translation::query();
        }

        return $this->topic->translations()->getQuery();
    }

    public function map($row): array
    {
        return [
            $row->en,
            $row->kz,
            $row->topics_string,
        ];
    }
}
