<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = ['status_id'];

    public function translations(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
    	return $this->belongsToMany('App\Translation');
    }
}
