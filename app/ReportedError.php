<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ReportedError extends Model
{
    public function translation()
    {
        return Translation::find($this->translation_id);
    }
    public function user()
    {
        return User::find($this->user_id);
    }
    public function status()
    {
        $row = DB::table('error_statuses')->where('id', $this->error_status_id)->first();
        return $row->name;
    }
}
