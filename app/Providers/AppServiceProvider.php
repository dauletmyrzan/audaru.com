<?php

namespace App\Providers;

use App\Page;
use App\Settings;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (!Schema::hasTable('settings')) {
            return;
        }

        $settings = DB::table('settings')->first();

        View::share('settings', $settings);

        Blade::directive('seo', function () use ($settings) {
            try {
                $keywords    = \Session::get('locale') == 'kz' ? $settings->keywords : $settings->keywords_en;
                $title       = \Session::get('locale') == 'kz' ? $settings->title : $settings->title_en;
                $image       = asset('uploads/img/' . $settings->image_src);
                $description = \Session::get('locale') == 'kz' ? $settings->description : $settings->description_en;

                return '<meta name="keywords" content="' . $keywords . '">
                    <meta name="description" content="Қазақ ағылшын тілдерінің тегін және онлайн сөйлем аудару қызметі. Бесплатный онлайн переводчик английский казахский языков и словарь. Онлайн сөздік">
                    <title>' . $title . '</title>
                    <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
                    <meta property="og:title" content="' . $title . '">
                    <meta property="og:url" content="' . $settings->url . '">
                    <meta property="og:type" content="website">
                    <meta property="og:image" content="' . $image .'">
                    <meta property="og:description" content="' . $description . '">
                    <meta name="twitter:card" content="summary_large_image">';
            } catch (\Throwable $e) {
                // Ничего
            }

            return '';
        });
    }
}
