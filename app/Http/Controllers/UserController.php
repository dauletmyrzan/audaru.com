<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): \Illuminate\View\View
    {
        $selectedRole = $request->role;

        if ($selectedRole === 'all') {
            $selectedRole = null;
        }

        $roles      = Role::all();
        $rolesTrans = [
            'admin'  => 'Администратор',
            'editor' => 'Редактор',
            'seo'    => 'SEO-специалист',
        ];

        foreach ($roles as $role) {
            $role->name = $rolesTrans[$role->name] ?? '';
        }

        $users = User::query()->when($selectedRole, function ($query) use ($selectedRole) {
            return $query->whereHas('roles', function ($q) use ($selectedRole) {
                $q->where('id', $selectedRole);
            });
        })->paginate(50);

        return view('admin.users.index', compact(['users', 'roles']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $user        = $request->id ? User::query()->find($request->id) : \Auth::user();
        $uri         = $request->getRequestUri();
        $permissions = Permission::all();
        $roles       = Role::all();
        $rolesTrans  = [
            'admin'  => 'Администратор',
            'editor' => 'Редактор',
            'seo'    => 'SEO-специалист',
        ];

        foreach ($roles as $role) {
            $role->title = $rolesTrans[$role->name] ?? '';
        }

        $view = strpos($uri, '/admin/users') !== false ? 'admin.users.show' : 'profile';

        return view($view, compact(['user', 'roles', 'permissions']));
    }

    public function update(Request $request)
    {
        $user          = User::query()->find($request->get('id'));
        $user->name    = $request->get('name');
        $user->surname = $request->get('surname');
        if (!in_array('no', $request->roles)) {
            $user->syncRoles($request->roles);
        } else {
            if ($user->id != \Auth::user()->id) {
                $user->roles()->detach();
                $user->forgetCachedPermissions();
            } else {
                return back()->withErrors(['message' => 'Вы администратор и хотите себя убрать из админов? Это должен сделать другой администратор.']);
            }
        }
        $user->save();

        return back()->with('message', 'Profile was successfully updated.');
    }

    public function updateProfile(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name'    => 'required|regex:/^[а-яА-Яa-zA-Z]+$/u|max:255',
            'surname' => 'regex:/^[а-яА-Яa-zA-Z]+$/u|max:255',
        ]);
        $user          = \Auth::user();
        $user->name    = $request->get('name');
        $user->surname = $request->get('surname');
        $user->save();

        return back()->with('message', 'Profile was successfully updated.');
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;

        if (count($ids) >= User::all()->count()) {
            return response()->json([
                'success' => false,
                'message' => 'В базе должен остаться хотя бы один пользователь, иначе вы не зайдете.'
            ]);
        }

        User::query()->whereIn('id', $ids)->delete();

        return response()->json([
            'success' => true,
        ]);
    }

    public function deleteUser(Request $request)
    {
        $id   = $request->get('id');
        $user = User::query()->find($id);
        $user->delete();

        return redirect('/admin/users')->with('message', 'Пользователь удален.');
    }

    public function authById(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        Auth::loginUsingId($id);

        return redirect('/admin');
    }
}
