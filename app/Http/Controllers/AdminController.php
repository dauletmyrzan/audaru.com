<?php
namespace App\Http\Controllers;

use App\Topic;
use App\Status;
use App\Translation;
use App\TranslationEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * Показывает форму для редактирования перевода
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getEditForm(Request $request): \Illuminate\Http\JsonResponse
    {
        $id          = $request->get('id');
        $translation = Translation::query()->find($id);
        $topics      = Topic::all();

        return response()->json([
            'success' => true,
            'html'    => view('include.forms.edit_translation', compact(['translation', 'topics']))->render(),
        ]);
    }

    /**
     * Показывает корзину в админ. панели
     *
     * @return \Illuminate\View\View
     */
    public function basket(): \Illuminate\View\View
    {
        $title        = 'Корзина';
        $status       = Status::query()->where('name', 'В корзине')->first();
        $topics       = Topic::query()->where('status_id', $status->id)->paginate(15);
        $translations = Translation::where('status_id', $status->id)->orderBy('created_at', 'DESC')->paginate(15);

        return view('admin.basket', compact(['translations', 'title', 'topics']));
    }

    /**
     * Показывает страницу статистики, а именно список всех поисковых запросов
     *
     * @return \Illuminate\View\View
     */
    public function stats(): \Illuminate\View\View
    {
        $stats = DB::table('queries')->orderBy('id', 'desc')->paginate(15);

        return view('admin.stats.index', compact(['stats']));
    }

    /**
     * Показывает страницу статистики, а именно список частых поисковых запросов
     *
     * @return \Illuminate\View\View
     */
    public function statsFrequent(): \Illuminate\View\View
    {
        $stats = DB::table('queries')->select('query', DB::raw('count(*) as count'))
            ->groupBy('query')->orderBy('count', 'desc')->havingRaw('count(*) > 1')
            ->paginate(15);

        return view('admin.stats.frequent', compact(['stats']));
    }

    /**
     * Достает данные статистики по датам и возвращает их в виде json
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getData(Request $request): \Illuminate\Http\JsonResponse
    {
        $start_date = $request->get('start_date');
        $end_date   = $request->get('end_date');
        $page       = $request->get('page');

        if ($page === 'stats') {
            $stats = DB::table('queries')
                ->orderBy('id', 'desc')
                ->whereBetween('created_at', [$start_date, $end_date])
                ->paginate(15);
        } elseif ($page == 'frequent') {
            $stats = DB::table('queries')->select('query', DB::raw('count(*) as count'))
                ->groupBy('query')
                ->whereBetween('created_at', [$start_date, $end_date])
                ->orderBy('count', 'desc')
                ->havingRaw('count(*) > 1')
                ->paginate(15);
        }

        return response()->json([
            'success' => true,
            'html'    => view('ajax.' . $page, compact(['stats']))->render(),
        ]);
    }

    /**
     * Очишает корзину, удалив записи из таблиц translations и topics
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clearBasket(): \Illuminate\Http\RedirectResponse
    {
        $status = Status::query()->where('name', 'В корзине')->first();

        Topic::query()->where('status_id', $status->id)->delete();
        Translation::where('status_id', $status->id)->delete();

        return back()->with('message', 'Корзина успешно очищена.');
    }

    public function roles()
    {
        $roles = Role::all();

        return view('admin.roles.index', compact(['roles']));
    }

    public function editRole(Request $request, int $id)
    {
        $role        = Role::query()->find($id);
        $permissions = Permission::all();

        if (!$role) {
            abort(404);
        }

        return view('admin.roles.edit', compact(['role', 'permissions']));
    }

    public function updateRole(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        /** @var \Spatie\Permission\Models\Role $role */
        $role        = Role::query()->find($id);
        $permissions = Permission::query()->whereIn('id', $request->permissions)->pluck('name')->toArray();

        if (!$role) {
            abort(404);
        }

        if ($role->name === 'admin') {
            return redirect()->route('roles');
        }

        $role->name = $request->name;
        $role->givePermissionTo($permissions);
        $role->save();

        return redirect()->route('roles');
    }

    public function permissions()
    {
        $permissions = Permission::all();

        return view('admin.permissions.index', compact(['permissions']));
    }

    public function editPermission(Request $request, int $id)
    {
        $permission = Permission::query()->find($id);

        if (!$permission) {
            abort(404);
        }

        return view('admin.permissions.edit', compact(['permission']));
    }

    public function updatePermission(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $permission = Permission::query()->find($id);

        if (!$permission) {
            abort(404);
        }

        if ($permission->name === 'admin') {
            return redirect()->route('permissions');
        }

        $permission->name = $request->name;
        $permission->save();

        return redirect()->route('permissions');
    }

    public function clearStats()
    {
        DB::table('queries')->where('created_at', '<', Carbon::now()->subYears(3))->delete();

        return back();
    }
}
