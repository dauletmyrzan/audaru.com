<?php

namespace App\Http\Controllers;

use App\Seo;
use App\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $randomPhrases = $this->generateRandomPhrases();
        $seoObj        = Seo::query()->where('path', '/')->first();
        $seo           = '';

        if ($seoObj) {
            $seo = Session::get('locale') === 'en' ? $seoObj->code_en : $seoObj->code_kz;
        }

        return view('home', compact(['randomPhrases', 'seo']));
    }

    /**
     * Вызывает функцию generateRandomPhrases и возвращает ее результат в виде json
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomPhrases(): \Illuminate\Http\JsonResponse
    {
        $randomPhrases = $this->generateRandomPhrases();

        return response()->json([
            'success' => true,
            'phrases' => $randomPhrases,
        ]);
    }

    /**
     * Возвращает 2 рандомные фразы из таблицы translations
     *
     * @return array
     */
    public function generateRandomPhrases(): array
    {
        $translations  = DB::select('SELECT en, kz, qaz FROM translations AS r1 JOIN
                                        (SELECT CEIL(RAND() *
                                            (SELECT MAX(id)
                                                FROM translations)) AS id)
                                                AS r2
                                         WHERE r1.id >= r2.id
                                         AND r1.qaz <> ""
                                         AND r1.qaz IS NOT NULL
                                         ORDER BY r1.id ASC
                                         LIMIT 2');
        $randomPhrases = [];
        foreach ($translations as $item) {
            $randomPhrases['kz'][]  = "<a href='#'>" . trim($item->kz) . "</a>";
            $randomPhrases['en'][]  = "<a href='#'>" . trim($item->en) . "</a>";
            $randomPhrases['qaz'][] = "<a href='#'>" . trim($item->qaz) . "</a>";
        }

        return $randomPhrases;
    }

    /**
     * Показывает страницу конвертера. Deprecated.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function converter()
    {
        return view('converter');
    }

    /**
     * Конвертирует кириллицу в латиницу
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function convert(Request $request): \Illuminate\Http\JsonResponse
    {
        $text        = $request->get('text');
        $translation = new Translation;

        return response()->json([
            'converted' => $translation->transliterate($text),
        ]);
    }

    /**
     * Показывает страницу "О нас"
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        $seoObj = Seo::query()->where('path', '/about')->first();
        $seo    = '';

        if ($seoObj) {
            $seo = Session::get('locale') === 'en' ? $seoObj->code_en : $seoObj->code_kz;
        }

        return view('about', compact('seo'));
    }

    /**
     * Показывает страницу "Контакты"
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contacts()
    {
        $seoObj = Seo::query()->where('path', '/contacts')->first();
        $seo    = '';

        if ($seoObj) {
            $seo = Session::get('locale') === 'en' ? $seoObj->code_en : $seoObj->code_kz;
        }

        return view('contacts', compact('seo'));
    }

    /**
     * Показывает страницу "Обучение"
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cards()
    {
        $seoObj = Seo::query()->where('path', '/cards')->first();
        $seo    = '';

        if ($seoObj) {
            $seo = Session::get('locale') === 'en' ? $seoObj->code_en : $seoObj->code_kz;
        }

        return view('cards', compact('seo'));
    }

    /**
     * Возвращает 30 рандомных слов для карточки на странице "Обучение"
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCardWords()
    {
        $rows = DB::select("SELECT t.id FROM translations t
	    JOIN (SELECT RAND() * (SELECT MAX(id) FROM translations) AS max_id ) AS m
	    WHERE t.id >= m.max_id
	    ORDER BY t.id ASC
	    LIMIT 30;");
        $ids  = [];
        foreach ($rows as $row) {
            $ids[] = $row->id;
        }
        $translations = Translation::whereIn('id', $ids)->get();

        return response()->json([
            'success' => 'true',
            'data'    => $translations,
        ]);
    }

    /**
     * Устанавливает язык.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setLocale(Request $request, string $locale): \Illuminate\Http\RedirectResponse
    {
        if (in_array($locale, Config::get('app.locales'))) {
            Session::put('locale', $locale);
        }

        return back();
    }

    public function policy()
    {
        return view('policy');
    }
}
