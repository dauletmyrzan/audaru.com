<?php

namespace App\Http\Controllers;

use App\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * Функция поиска переводов
     *
     * @param  Request    $request
     * @throws \Throwable
     */
    public function search(Request $request)
    {
        $langFrom     = $request->get('lang_from');
        $langTo       = preg_replace('/[^a-z]/', '', $request->get('lang_to'));
        $query        = preg_replace("/[^0-9A-Za-zА-Яа-яӘәІіҢңҒғҮүҰұҚқӨөҺһ()—*.,\-\s]*$/u", '', $request->get('query'));
        $alternatives = getAlternatives($query);
        $translations = Translation::where('status_id', 1)
            ->where(function ($q) use ($langFrom, $query) {
                $q->where($langFrom, $query)
                    ->orWhere($langFrom, 'like', $query . ' %');
            })
            ->orWhere(function ($q) use ($langFrom, $alternatives) {
                foreach ($alternatives as $alt) {
                    $q->orWhere($langFrom, $alt)
                        ->orWhere($langFrom, 'like', $alt . ' %');
                }
            })
            ->orderBy('rating', 'desc')
            ->orderBy($langFrom, 'asc')
            ->orderBy($langTo, 'asc')
            ->get()->groupBy($langFrom);
        DB::table('queries')->insert([
            'query'      => $query,
            'remote_ip'  => $request->ip(),
            'user_id'    => \Auth::check() ? \Auth::user()->id : null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        $history = $request->session()->get('history') ?? [];
        if (!in_array($query, $history)) {
            $request->session()->push('history', $query);
        }

        $json = json_encode([
            'success' => true,
            'message' => $translations,
            'html'    => view('ajax.result', compact(['query', 'langTo', 'langFrom', 'translations']))->render(),
        ]);
        echo $json;
    }

    /**
     * Функция для автодополнения к полю поиска
     *
     * @param Request $request
     */
    public function searchAutocomplete(Request $request)
    {
        $query        = preg_replace("/[^0-9A-Za-zА-Яа-яӘәІіҢңҒғҮүҰұҚқӨөҺһ —*,\-\s]*$/u", '', $request->get('query'));
        $langFrom     = $request->get('lang_from');
        $alternatives = getAlternatives($query);
        $translations = Translation::where('status_id', 1)
            ->where($langFrom, 'like', $query . '%')->distinct($langFrom)
            ->orWhere(function ($q) use ($langFrom, $alternatives) {
                foreach ($alternatives as $alt) {
                    $q->orWhere($langFrom, $alt)
                        ->orWhere($langFrom, 'like', $alt . ' %');
                }
            })
            ->orderBy('rating', 'desc')
            ->orderBy($langFrom, 'asc')->limit(7)
            ->pluck($langFrom)->toArray();
        echo json_encode($translations);
    }
}
