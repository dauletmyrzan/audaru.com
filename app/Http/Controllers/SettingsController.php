<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::first();
        return view('admin.settings.index', compact(['settings']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $color_scheme = $request->get('color_scheme');
        $color_names = [
            '#2ca8ff' => 'blue',
            '#f96332' => 'orange',
            '#ff3636' => 'red',
            '#8bc34a' => 'green'
        ];
        $color_scheme_name = $color_names[$color_scheme];
        if($request->has('image_src'))
        {
            $file = request()->file('image_src');
            if(!$file->storeAs('img', $file->getClientOriginalName(), 'public_uploads'))
            {
                echo "Ошибка загрузки файла!";
                exit;
            }
            $image_src = $file->getClientOriginalName();
        }
        else
        {
            $settings = Settings::where('id', 1)->first();
            $image_src = $settings->image_src;
        }
        Settings::where('id', 1)->update([
            'color_scheme' => $color_scheme,
            'color_scheme_name' => $color_scheme_name,
            'title' => $request->get('title'),
            'title_en' => $request->get('title_en'),
            'keywords' => $request->get('keywords'),
            'keywords_en' => $request->get('keywords_en'),
            'description_en' => $request->get('description_en'),
            'image_src' => $image_src,
            'url' => $request->get('url'),
        ]);
        return back()->with('message', 'Настройки обновлены.');
    }
}
