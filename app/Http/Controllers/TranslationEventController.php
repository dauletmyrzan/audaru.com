<?php

namespace App\Http\Controllers;

use App\TranslationEvent;
use Illuminate\Support\Carbon;

/**
 * Class TranslationEventController
 *
 * @package App\Http\Controllers
 */
class TranslationEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(): \Illuminate\View\View
    {
        $events = TranslationEvent::query()->orderBy('created_at', 'desc')->paginate(20);

        return view('admin.journal.index', compact(['events']));
    }

    /**
     * Удаляет записи старше 6 месяцев.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear(): \Illuminate\Http\RedirectResponse
    {
        TranslationEvent::query()->where('created_at', '<',  Carbon::now()->subMonths(6))->delete();

        return back();
    }
}
