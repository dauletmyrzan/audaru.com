<?php

namespace App\Http\Controllers;

use App\Exports\TranslationsExport;
use App\Topic;
use App\Status;
use App\Translation;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::where('status_id', '<>', 4)->paginate(25);

        return view('admin.topics.index', compact(['topics']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topic          = new Topic;
        $topic->name    = $request->get('name');
        $topic->name_kz = $request->get('name_kz');
        $topic->save();

        return redirect('/admin/topics');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id    = $request->id;
        $topic = Topic::find($id);

        return view('admin.topics.edit', compact(['topic']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Topic               $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $topic          = Topic::find($request->get('id'));
        $topic->name    = $request->get('name');
        $topic->name_kz = $request->get('name_kz');
        $topic->save();

        return back()->with('message', 'Обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $topic = Topic::find($request->get('id'));
        if (!$request->has('permanent')) {
            $status           = Status::where('name', 'В корзине')->first();
            $topic->status_id = $status->id;
            $topic->save();
        } else {
            $topic->translations()->detach();
            $topic->delete();
        }

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Удаляет словарь темы.
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteWithTranslations(Request $request): \Illuminate\Http\JsonResponse
    {
        $topic = Topic::query()->find($request->id);

        if (!$topic) {
            return response()->json([
                'success' => false,
            ]);
        }

        $topic->translations()->delete();

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Восстанавливает тематику из корзины
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(Request $request)
    {
        $id     = $request->get('id');
        $status = Status::where('name', 'Активно')->first();
        Topic::find($id)->update(['status_id' => $status->id]);

        return response()->json([
            'success' => true,
            'message' => 'Тематика восстановлена!',
        ]);
    }

    public function exportIndex()
    {
        $topics = Topic::query()->where('status_id', '<>', 4)->get();

        return view('admin.export.index', compact(['topics']));
    }

    public function export(Request $request)
    {
        ini_set('max_execution_time', '0');
        $topic = Topic::query()->find($request->topic);

        if (!$topic && $request->topic !== 'all') {
            return back();
        }

        $fileName = $request->topic === 'all' ? 'База audaru.com' : $topic->name;

        return (new TranslationsExport($topic))->download($fileName . '.xlsx');
    }
}
