<?php

namespace App\Http\Controllers;

use App\Page;
use App\Seo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    /**
     * Показывает индексную страницу страниц
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function show(Request $request): \Illuminate\View\View
    {
        $uri  = $request->getPathInfo();
        $page = Page::query()->where('uri', $uri)->first();

        if (!$page) {
            abort(404);
        }

        $view   = ($page->uri === '/contacts') ? 'contacts' : 'page';
        $seoObj = Seo::query()->where('path', $uri)->first();
        $seo    = '';

        if ($seoObj) {
            $seo = Session::get('locale') === 'en' ? $seoObj->code_en : $seoObj->code_kz;
        }

        return view($view, compact(['page', 'seo']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if (strpos($request->getRequestUri(), '/about') !== false) {
            $uri = '/about';
        } elseif (strpos($request->getRequestUri(), '/contacts') !== false) {
            $uri = '/contacts';
        } else {
            $uri = 'footer_description';
        }
        $page = Page::where('uri', $uri)->first();

        return view('admin.pages.show', compact(['page']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $page             = Page::find($request->get('id'));
        $page->title_kz   = $request->get('title_kz');
        $page->title_en   = $request->get('title_en');
        $page->content_kz = $request->get('content_kz');
        $page->content_en = $request->get('content_en');
        $page->save();

        return redirect('/admin/pages/')->with('message', 'Данные обновлены');
    }
}
