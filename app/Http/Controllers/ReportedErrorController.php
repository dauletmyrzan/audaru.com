<?php

namespace App\Http\Controllers;

use App\ReportedError;
use Illuminate\Http\Request;

class ReportedErrorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reportedErrors = ReportedError::paginate(15);
        return view('admin.reported-errors.index', compact(['reportedErrors']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = new ReportedError;
        $error->translation_id = $request->get('id') ?? null;
        $error->selected_text = $request->get('selected_text');
        $error->message = $request->get('message');
        $error->user_id = \Auth::user()->id ?? null;
        $error->error_status_id = 2;
        $error->save();
        $msg = \Session::get('locale') == 'kz' ? 'Рахмет! Сіздің хабарламаңыз жіберілді.' : 'Thank you! Your message successfully sent.';
        return response()->json([
            'success' => true,
            'message' => '<div class="h5">' . $msg . '</div>'
        ]);
    }
}
