<?php
namespace App\Http\Controllers;

use App\Seo;
use App\Topic;
use App\Status;
use App\Translation;
use App\TranslationEvent;
use Illuminate\Http\Request;
use App\Imports\TranslationsImport;
use Maatwebsite\Excel\Facades\Excel;

class TranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): \Illuminate\View\View
    {
        $topics   = Topic::query()->orderBy('name')->get();
        $statuses = Status::query()->where('name', '<>', 'В корзине')->get();

        if ($request->has('query')) {
            $q           = preg_replace("/[^A-Za-zА-Яа-яӘәІіҢңҒғҮүҰұҚқӨөҺһ0-9\s,-]/u", '', $request->get('query'));
            $translation = Translation::query()->where(function ($query) use ($q) {
                $query
                    ->where('kz', 'like', $q . '%')
                    ->orWhere('qaz', 'like', $q . '%')
                    ->orWhere('en', 'like', $q . '%');
            })
                ->where('status_id', '<>', 4)
                ->limit(10)
                ->paginate(30);

            return view('admin.translations.index', compact(['translation', 'topics', 'statuses']));
        }

        $selectedTopic  = $request->get('topic');

        if ($selectedTopic === 'all') {
            $selectedTopic = null;
        }

        $selectedStatus = $request->get('status');
        $translation    = Translation::query()->sortable()->when($selectedTopic, function ($query) use ($selectedTopic) {
                return $query->whereHas('topics', function ($q) use ($selectedTopic) {
                    $q->where('id', $selectedTopic);
                });
            })
            ->when($selectedStatus, function ($query) use ($selectedStatus) {
                $query->where('status_id', $selectedStatus);
            })
            ->where('status_id', '<>', 4)
            ->where(function ($query) {
                $query->where('suggested', 0)->orWhereNull('suggested');
            })
            ->paginate(25);

        return view('admin.translations.index', compact(['translation', 'topics', 'statuses']));
    }

    /**
     * Показывает страницу с предложенными пользователями словами
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function indexUserPhrases(Request $request): \Illuminate\View\View
    {
        $selectedTopic = $request->get('topic');
        $topics   = Topic::query()->orderBy('name')->get();
        $statuses      = Status::query()->where('name', '<>', 'В корзине')->get();
        $status        = Status::query()->where('name', 'Активно')->first();
        $translation   = Translation::query()->when($selectedTopic, function ($query) use ($selectedTopic) {
            return $query->whereHas('topics', function ($q) use ($selectedTopic) {
                $q->where('id', $selectedTopic);
            });
        })
            ->where('status_id', '<>', 4)
            ->where('suggested', 1)
            ->orderBy('created_at', 'DESC')->paginate(10);

        return view('admin.translations.index', compact(['translation', 'topics', 'statuses']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): \Illuminate\View\View
    {
        $topics = Topic::query()->orderBy('name')->get();

        return view('admin.translations.create', compact(['topics']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request                                         $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'kz'     => 'required',
            'qaz'    => 'required',
            'en'     => 'required',
            'rating' => 'required',
        ]);
        $translation         = new Translation;
        $translation->kz     = $request->get('kz');
        $translation->en     = $request->get('en');
        $translation->qaz    = $request->get('qaz');
        $translation->rating = $request->get('rating');
        $translation->save();
        $translation->topics()->attach($request->get('topics'));

        return redirect('/admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                   $id
     * @return \Illuminate\View\View
     */
    public function edit(int $id): \Illuminate\View\View
    {
        $phrase   = Translation::query()->find($id);
        $topics   = Topic::query()->orderBy('name')->get();
        $statuses = Status::all();

        return view('admin.translations.edit', compact(['phrase', 'topics', 'statuses']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request                                     $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $request->validate([
            'kz'     => 'required',
            'qaz'    => 'required',
            'en'     => 'required',
            'rating' => 'required',
            'topics' => 'required',
        ]);
        $translation            = Translation::query()->find($request->get('id'));
        $translation->kz        = $request->get('kz');
        $translation->en        = $request->get('en');
        $translation->qaz       = $request->get('qaz');
        $translation->rating    = $request->get('rating');
        $translation->status_id = $request->get('status') ?? 1;
        $translation->topics()->sync($request->get('topics'));
        $translation->save();
        $event                 = new TranslationEvent;
        $event->translation_id = $translation->id;
        $event->kz             = $translation->kz;
        $event->en             = $translation->en;
        $event->qaz            = $translation->qaz;
        $event->rating         = $translation->rating;
        $event->status_id      = $translation->status_id;
        $event->author_id      = \Auth::user()->id;
        $event->save();
        $msg = 'Обновлено!';

        return !$request->ajax() ? redirect($request->get('redirect'))->with('message', $msg) : response()->json([
            'success' => true,
            'message' => $msg,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request): \Illuminate\Http\JsonResponse
    {
        if ($request->has('ids')) {
            $translations = Translation::whereIn('id', $request->get('ids'))->get();
            if (!$request->has('permanent')) {
                $status = Status::query()->where('name', 'В корзине')->first();
                foreach ($translations as $translation) {
                    $translation->status_id = $status->id;
                    $translation->save();
                    $event                 = new TranslationEvent;
                    $event->translation_id = $translation->id;
                    $event->kz             = $translation->kz;
                    $event->en             = $translation->en;
                    $event->qaz            = $translation->qaz;
                    $event->rating         = $translation->rating;
                    $event->status_id      = $translation->status_id;
                    $event->author_id      = \Auth::user()->id;
                    $event->save();
                }
            } else {
                foreach ($translations as $translation) {
                    $translation->topics()->detach();
                    $event                 = new TranslationEvent;
                    $event->translation_id = $translation->id;
                    $event->kz             = $translation->kz;
                    $event->en             = $translation->en;
                    $event->qaz            = $translation->qaz;
                    $event->rating         = $translation->rating;
                    $event->status_id      = $translation->status_id;
                    $event->author_id      = \Auth::user()->id;
                    $event->deleted        = true;
                    $event->save();
                    $translation->delete();
                }
            }
        } else {
            $translation = Translation::query()->find($request->get('id'));
            if (!$request->has('permanent')) {
                $status                 = Status::query()->where('name', 'В корзине')->first();
                $translation->status_id = $status->id;
                $translation->save();
                $event                 = new TranslationEvent;
                $event->translation_id = $translation->id;
                $event->kz             = $translation->kz;
                $event->en             = $translation->en;
                $event->qaz            = $translation->qaz;
                $event->rating         = $translation->rating;
                $event->status_id      = $translation->status_id;
                $event->author_id      = \Auth::user()->id;
                $event->save();
            } else {
                $translation->topics()->detach();
                $event                 = new TranslationEvent;
                $event->translation_id = $translation->id;
                $event->kz             = $translation->kz;
                $event->en             = $translation->en;
                $event->qaz            = $translation->qaz;
                $event->rating         = $translation->rating;
                $event->status_id      = $translation->status_id;
                $event->author_id      = \Auth::user()->id;
                $event->deleted        = true;
                $event->save();
                $translation->delete();
            }
        }

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Функция поиска слов в админ. панели
     *
     * @param  \Illuminate\Http\Request      $request
     * @throws \Throwable
     */
    public function search(Request $request)
    {
        $query       = preg_replace("/[^A-Za-zА-Яа-яӘәІіҢңҒғҮүҰұҚқӨөҺһ0-9\s,-]/u", '', $request->get('query'));
        $translation = Translation::where('kz', 'like', $query . '%')
            ->orWhere('qaz', 'like', $query . '%')
            ->orWhere('en', 'like', $query . '%')->limit(10)->paginate(30);

        return view('admin.translations.index', compact(['translation']));

//        return response()->json($json);
    }

    /**
     * Восстанавливает перевод из корзины
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(Request $request): \Illuminate\Http\JsonResponse
    {
        $id                     = $request->get('id');
        $status                 = Status::query()->where('name', 'Активно')->first();
        $translation            = Translation::query()->find($id);
        $translation->status_id = $status->id;
        $translation->save();
        $event                 = new TranslationEvent;
        $event->translation_id = $translation->id;
        $event->kz             = $translation->kz;
        $event->en             = $translation->en;
        $event->qaz            = $translation->qaz;
        $event->rating         = $translation->rating;
        $event->status_id      = $translation->status_id;
        $event->author_id      = \Auth::user()->id;
        $event->save();

        return response()->json([
            'success' => true,
            'message' => 'Перевод восстановлен!',
        ]);
    }

    /**
     * Проверка на наличие перевода по запросу
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkTranslation(Request $request): \Illuminate\Http\JsonResponse
    {
        $query        = $request->get('query');
        $translations = Translation::where('en', $query)
            ->orWhere('en', 'like', $query . '%')
            ->orWhere('en', 'like', '% ' . $query)
            ->orWhere('kz', $query)
            ->orWhere('kz', 'like', $query . '%')
            ->orWhere('kz', 'like', '% ' . $query)
            ->orWhere('qaz', $query)
            ->orWhere('qaz', 'like', $query . '%')
            ->orWhere('qaz', 'like', '% ' . $query)
            ->get(['id']);

        return response()->json([
            'success' => $translations->count() > 0,
            'message' => $translations->count() > 0 ? 'Перевод существует!' : 'Перевод отсутствует!',
        ]);
    }

    /**
     * Показывает страницу импорта файла
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showImport()
    {
        return view('admin.import');
    }

    /**
     * Загружает файл
     *
     * @param  \Illuminate\Http\Request                   $request
     * @return \Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function uploadBase(Request $request): \Illuminate\View\View
    {
        $request->validate([
            'file' => 'required|file|max:1000000',
        ]);
        $extensions    = ["xls", "xlsx"];
        $file          = $request->file('file');
        $fileExtension = $file->getClientOriginalExtension();
        $fileName      = $file->getClientOriginalName();
        if (in_array($fileExtension, $extensions)) {
            $path = $request->file('file')->storeAs('base', $fileName, 'public');

            return view('admin.import', compact(['path', 'fileName']));
        } else {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'file' => 'Файл должен быть формата .xlsx',
            ]);
        }
    }

    /**
     * Функция начала импорта слов в базу
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function startImport(Request $request): \Illuminate\Http\JsonResponse
    {
        $fileName = $request->get('fileName');
        $import   = new TranslationsImport;
        Excel::import($import, 'base/' . $fileName, 'public');
        if ($import->data['success']) {
            return response()->json([
                'success' => true,
                'message' => $import->data['message'],
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => $import->data['message'] ?? "Импорт завершился с ошибкой!",
            ]);
        }
    }

    /**
     * Конвертирует кириллицу в латиницу
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function transliterateAjax(Request $request): \Illuminate\Http\JsonResponse
    {
        $word        = $request->get('word');
        $translation = new Translation;

        return response()->json([
            'success'        => true,
            'transliterated' => $translation->transliterate($word),
        ]);
    }

    /**
     * Возвращает список запросов, сделанных пользователем
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRequestsHistory(Request $request): \Illuminate\Http\JsonResponse
    {
        $history = $request->session()->get('history');

        if (is_array($history) && count($history) > 3) {
            $history = array_slice($history, -7, 7, true);
        }

        return response()->json($history);
    }

    /**
     * Страница добавления слов пользователем
     *
     * @return \Illuminate\View\View
     */
    public function addIndex(): \Illuminate\View\View
    {
        $seoObj = Seo::query()->where('path', '/translation/add')->first();
        $seo    = '';

        if ($seoObj) {
            $seo = \Session::get('locale') === 'kz' ? $seoObj->code_kz : $seoObj->code_en;
        }

        return view('translation.add', compact('seo'));
    }

    /**
     * Создаем новый объект класса Translation и сохраняет его со статусом "На модерации"
     *
     * @param  \Illuminate\Http\Request          $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request): \Illuminate\Http\RedirectResponse
    {
        $translation = new Translation;
        $request->validate([
            'kz'  => 'required',
            'en'  => 'required',
            'qaz' => 'required',
        ]);
        $translation->kz        = $request->get('kz');
        $translation->en        = $request->get('en');
        $translation->qaz       = $request->get('qaz');
        $status                 = Status::where('name', 'На модерации')->first();
        $translation->suggested = 1;
        $translation->status_id = $status->id;
        $translation->user_id   = \Auth::user()->id;
        $translation->save();

        return back()->with("message", "Ваш перевод отправлен на модерацию. Вы можете посмотреть свои переводы в <a href='/profile'>личном кабинете</a>");
    }

    /**
     * Утверждает Translation, меняя статус на "Активно"
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(Request $request): \Illuminate\Http\JsonResponse
    {
        $id                     = $request->get('id');
        $translation            = Translation::query()->find($id);
        $translation->status_id = 1;
        $event                  = new TranslationEvent;
        $event->translation_id  = $translation->id;
        $event->kz              = $translation->kz;
        $event->en              = $translation->en;
        $event->qaz             = $translation->qaz;
        $event->rating          = $translation->rating;
        $event->status_id       = $translation->status_id;
        $event->author_id       = \Auth::user()->id;
        $event->save();
        $translation->save();

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Отклоняет Translation, меняя статус на "Отклонено"
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function decline(Request $request): \Illuminate\Http\JsonResponse
    {
        $id                     = $request->get('id');
        $translation            = Translation::query()->find($id);
        $translation->status_id = 3;
        $event                  = new TranslationEvent;
        $event->translation_id  = $translation->id;
        $event->kz              = $translation->kz;
        $event->en              = $translation->en;
        $event->qaz             = $translation->qaz;
        $event->rating          = $translation->rating;
        $event->status_id       = $translation->status_id;
        $event->author_id       = \Auth::user()->id;
        $event->save();
        $translation->save();

        return response()->json([
            'success' => true,
        ]);
    }
}
