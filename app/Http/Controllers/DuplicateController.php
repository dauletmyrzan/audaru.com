<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DuplicateController extends Controller
{
    public function index(Request $request)
    {
        $duplicates = DB::table('translations as a')
            ->select('a.*')
            ->join(DB::raw('(SELECT en, kz, COUNT(*) totalCount
                                            FROM    translations
                                            GROUP   BY en, kz
                                            HAVING  totalCount > 1) as b'), function ($join) {
                /** @var \Illuminate\Database\Query\JoinClause $join */
                $join->on('a.en', '=', 'b.en')->on('a.kz', '=', 'b.kz');
            })
            ->orderByRaw('b.TotalCount DESC, a.en ASC')
            ->whereRaw('a.status_id <> 4')
            ->paginate();

        return view('admin.duplicates.index', compact('duplicates'));
    }
}
