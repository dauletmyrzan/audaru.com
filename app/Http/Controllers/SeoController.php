<?php
namespace App\Http\Controllers;

use App\Seo;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    public function index()
    {
        $seo = Seo::all();

        return view('admin.seo.index', compact('seo'));
    }

    public function edit(Request $request, int $id)
    {
        $seo = Seo::query()->find($id);

        return view('admin.seo.edit', compact('seo'));
    }

    public function update(Request $request)
    {
        $seo = Seo::query()->find($request->id);

        if (!$seo) {
            return back();
        }

        $text_en      = strip_tags($request->code_en, "<meta><title><link>");
        $text_kz      = strip_tags($request->code_kz, "<meta><title><link>");
        $seo->code_en = base64_encode($text_en);
        $seo->code_kz = base64_encode($text_kz);

        $seo->save();

        return back();
    }
}
