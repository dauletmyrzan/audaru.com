<?php

namespace App\Http\Middleware;

use App;
use Closure;
use App\Page;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $raw_locale = \Session::get('locale');

        if (in_array($raw_locale, Config::get('app.locales'))) {
            $locale = $raw_locale;
        } else {
            $locale = Config::get('app.locale');
        }

        Session::put('locale', $locale);
        \App::setLocale($locale);

        if (Schema::hasTable('pages')) {
            $footer_description = '';
            $page               = Page::where('uri', 'footer_description')->first();

            if ($page) {
                $footer_description = \App::getLocale() == 'kz' ? $page->content_kz : $page->content_en;
            }

            View::share('footer_description', $footer_description);
        }

        return $next($request);
    }
}
