<?php

namespace App\Console\Commands;

use App\Translation;
use Illuminate\Console\Command;

class Converter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'converter:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Converts cyrillic to latin qazaqsha';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Starting...\n";
        $translations = Translation::whereNull('qaz')->get();
        foreach($translations as $translation)
        {
            $translation->qaz = $this->transliterate($translation->kz);
            $translation->save();
        }
        echo "Converting finished!\n";
    }

    public function transliterate($string)
    {
        $converter = array(
            "а" => "a",
            "ә" => "á",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "ғ" => "ǵ",
            "д" => "d",
            "е" => "e",
            "ё" => "ıo",
            "ж" => "j",
            "з" => "z",
            "и" => "ı",
            "й" => "ı",
            "к" => "k",
            "қ" => "q",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "ң" => "ń",
            "о" => "o",
            "ө" => "ó",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "ý",
            "ұ" => "u",
            "ү" => "ú",
            "ф" => "f",
            "х" => "h",
            "һ" => "h",
            "ц" => "ts",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "sh",
            "ъ" => "",
            "ы" => "y",
            "і" => "i",
            "ь" => "",
            "э" => "e",
            "ю" => "ıý",
            "я" => "ıa",
            "А" => "A",
            "Ә" => "Á",
            "Б" => "B",
            "В" => "V",
            "Г" => "G",
            "Ғ" => "Ǵ",
            "Д" => "D",
            "Е" => "E",
            "Ё" => "IO",
            "Ж" => "J",
            "З" => "Z",
            "И" => "I",
            "Й" => "I",
            "К" => "K",
            "Қ" => "Q",
            "Л" => "L",
            "М" => "M",
            "Н" => "N",
            "Ң" => "Ń",
            "О" => "O",
            "Ө" => "Ó",
            "П" => "P",
            "Р" => "R",
            "С" => "S",
            "Т" => "T",
            "У" => "Ý",
            "Ұ" => "U",
            "Ү" => "Ú",
            "Ф" => "F",
            "Х" => "H",
            "Һ" => "H",
            "Ц" => "TS",
            "Ч" => "CH",
            "Ш" => "SH",
            "Щ" => "SH",
            "Ъ" => "",
            "Ы" => "Y",
            "І" => "I",
            "Ь" => "",
            "Э" => "E",
            "Ю" => "IÝ",
            "Я" => "IA"
        );
        return strtr($string, $converter);
    }
}
