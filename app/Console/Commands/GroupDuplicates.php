<?php

namespace App\Console\Commands;

use App\Translation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GroupDuplicates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duplicates:group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Группирует дубликаты по темам';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $topics = [];

        $translations = DB::table('translations as a')
            ->select('a.*')
            ->join(DB::raw('(SELECT en, kz, COUNT(*) totalCount
                                            FROM    translations
                                            GROUP   BY en, kz
                                            HAVING  totalCount > 1) as b'), function ($join) {
                /** @var \Illuminate\Database\Query\JoinClause $join */
                $join->on('a.en', '=', 'b.en')->on('a.kz', '=', 'b.kz');
            })
            ->orderByRaw('b.TotalCount DESC, a.en ASC')
            ->limit(5000)->get();

        foreach ($translations as $row) {
            $translation       = Translation::query()->findOrFail($row->id);
            $row->en           = mb_strtolower($row->en);
            $translationTopics = $translation->topics;

            if ($translationTopics->isNotEmpty()) {
                foreach ($translationTopics as $topic) {
                    if (!isset($topics[$row->en]) || !in_array($topic->id, $topics[$row->en])) {
                        $topics[$row->en][] = $topic->id;
                    }
                }
            }
        }

        $bar = $this->output->createProgressBar(count($topics));

        $bar->start();

        foreach ($topics as $topic => $ids) {
            $word = Translation::query()->where('en', $topic)->first();
            $word->topics()->attach($ids);
            $word->save();

            $others = Translation::query()->where('en', $topic)->where('id', '<>', $word->id)->get();

            foreach ($others as $other) {
                $other->topics()->detach();
                $other->delete();
            }

            $bar->advance();
        }

        $bar->finish();

        $this->output->success('Скрипт завершен.');

        return 1;
    }
}
