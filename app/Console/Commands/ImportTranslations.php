<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\TranslationsImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:translations {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports translations from excel to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->option('filename');
        if(!$filename)
        {
            echo "Option --filename missing.\n";
            exit;
        }
        if(Excel::import(new TranslationsImport, $filename.'.xlsx', 'public'))
        {
            echo "Import successfully completed!\n";
        }
        else
        {
            echo "Import failed!\n";
        }
    }
}
