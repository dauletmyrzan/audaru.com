<?php

namespace App\Console\Commands;

use App\Translation;
use Illuminate\Console\Command;

class SetGeneralTopic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:set-general-topic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Translation::query()->doesntHave('topics')->chunk(1000, function ($translations) {
            foreach ($translations as $translation) {
                $translation->topics()->attach(17);
            }
        });

        $this->output->success('Скрипт завершен!');

        return 1;
    }
}
