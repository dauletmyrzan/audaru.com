<?php

namespace App\Imports;

use App\Topic;
use App\Translation;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class TranslationsImport implements ToCollection
{
    public $topics;

    public $data;

    public function __construct()
    {
        $topics = Topic::all();

        foreach ($topics as $topic) {
            $this->topics[$topic->name] = $topic->id;
        }
    }

    public function collection(Collection $collection)
    {
        DB::beginTransaction();

        foreach ($collection as $key => $row) {
            if ($key == 0 || $row[0] == '' || $row[1] == '' || $row[2] == '') {
                continue;
            }

            $en = str_replace(['\'', '\t', '\n'], '', trim($row[0]));
            $kz = str_replace(['\'', '\t', '\n'], '', trim($row[1]));

            $existingTranslation = Translation::query()->where([
                ['en', '=', $en],
                ['kz', '=', $kz]
            ])->first();

            $topicIds = $this->getTopicIds($row[2], $key);

            if ($existingTranslation && !empty($topicIds)) {
                $existingTranslation->topics()->syncWithoutDetaching($topicIds);
                $existingTranslation->save();

                continue;
            }

            $translation      = new Translation;
            $translation->en  = $en;
            $translation->kz  = $kz;
            $translation->qaz = $translation->transliterate($kz);
            $translation->save();

            $translation->topics()->attach($topicIds);
        }

        DB::commit();

        $this->data = [
            "success" => true,
            "message" => "Импорт успешно завершен!",
        ];
    }

    public function getTopicIds(string $column, int $key): array
    {
        $topics   = array_map('trim', explode(',', $column));
        $topicIds = [];

        foreach ($topics as $topic) {
            if (!isset($this->topics[$topic])) {
                $this->data = [
                    "success" => false,
                    "message" => "Нет такой темы как «" . $topic . "» на строке " . ($key + 1) . ".<br>Исправьте и загрузите заново.",
                ];

                return [];
            } else {
                $topicId = $this->topics[$topic];
            }

            if (!in_array($topicId, $topicIds)) {
                $topicIds[] = $topicId;
            }
        }

        if (empty($topicIds)) {
            $topicIds[] = $this->topics['Общая'];
        }

        return $topicIds;
    }
}
