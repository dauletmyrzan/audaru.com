<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * @method static whereIn(string $string, array $id_arr)
 * @method static where(string $string, bool $true)
 */
class Translation extends Model
{
    use Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'en', 'kz', 'topic',
    ];

    protected $sortable = [
        'id',
        'en',
        'kz',
        'created_at',
        'updated_at',
    ];

    public function topics()
    {
        return $this->belongsToMany('App\Topic');
    }

    public function getTopicsStringAttribute()
    {
        $topics      = $this->topics()->get();
        $topicsArray = [];

        foreach ($topics as $topic) {
            $topicsArray[] = $topic->name;
        }

        return implode(', ', $topicsArray);
    }

    public function transliterate($word = false)
    {
        $converter = [
            "а" => "a",
            "ә" => "á",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "ғ" => "ǵ",
            "д" => "d",
            "е" => "e",
            "ё" => "ıo",
            "ж" => "j",
            "з" => "z",
            "и" => "ı",
            "й" => "ı",
            "к" => "k",
            "қ" => "q",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "ң" => "ń",
            "о" => "o",
            "ө" => "ó",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "ý",
            "ұ" => "u",
            "ү" => "ú",
            "ф" => "f",
            "х" => "h",
            "һ" => "h",
            "ц" => "ts",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "sh",
            "ъ" => "",
            "ы" => "y",
            "і" => "i",
            "ь" => "",
            "э" => "e",
            "ю" => "ıý",
            "я" => "ıa",
            "А" => "A",
            "Ә" => "Á",
            "Б" => "B",
            "В" => "V",
            "Г" => "G",
            "Ғ" => "Ǵ",
            "Д" => "D",
            "Е" => "E",
            "Ё" => "IO",
            "Ж" => "J",
            "З" => "Z",
            "И" => "I",
            "Й" => "I",
            "К" => "K",
            "Қ" => "Q",
            "Л" => "L",
            "М" => "M",
            "Н" => "N",
            "Ң" => "Ń",
            "О" => "O",
            "Ө" => "Ó",
            "П" => "P",
            "Р" => "R",
            "С" => "S",
            "Т" => "T",
            "У" => "Ý",
            "Ұ" => "U",
            "Ү" => "Ú",
            "Ф" => "F",
            "Х" => "H",
            "Һ" => "H",
            "Ц" => "TS",
            "Ч" => "CH",
            "Ш" => "SH",
            "Щ" => "SH",
            "Ъ" => "",
            "Ы" => "Y",
            "І" => "I",
            "Ь" => "",
            "Э" => "E",
            "Ю" => "IÝ",
            "Я" => "IA",
        ];

        return strtr($word ? $word : $this->kz, $converter);
    }

    public function status($plain = false)
    {
        $status = Status::find($this->status_id);

        if (!$status) {
            return '?';
        }

        $statusHtml = "";

        if ($status->id == 1) {
            $statusHtml = "<span class='font-weight-bold text-success'>" . $status->name . "</span>";
        } elseif ($status->id == 2) {
            $statusHtml = "<span class='font-weight-bold text-secondary'>" . $status->name . "</span>";
        } elseif ($status->id == 3) {
            $statusHtml = "<span class='font-weight-bold text-warning'>" . $status->name . "</span>";
        } elseif ($status->id == 3) {
            $statusHtml = "<span class='font-weight-bold text-danger'>" . $status->name . "</span>";
        } elseif ($status->id == 3) {
            $statusHtml = "<span class='font-weight-bold text-muted'>" . $status->name . "</span>";
        }

        return $plain ? $status->name : $statusHtml;
    }
}
