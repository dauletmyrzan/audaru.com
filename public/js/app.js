$(document).ready(function(){
    let body = $("body");
    let kazakhAlphabet = /^[0-9А-Яа-яA-Za-zӘәІіҢңҒғҮүҰұҚқӨөҺһ —*,\-\s]*$/;
    let qazaqAlphabet = /^[0-9A-Za-zÁǴIŃÓÚÝáǵıńóúý —*,\-\s]*$/;
    let englishAlphabet = /^[0-9A-Za-z-\s]*$/;
    let ctrlDown = false,
        ctrlKey = 17,
        cmdKey = 91,
        vKey = 86;
    let langFrom = $('select[name=lang_from]').val();
    let langTo = $("select[name=lang_to]").val();
    let searchInput = $(".search-input");
    let topSearchInput = searchInput.eq(0);
    let translateBtn = $("#translate_btn");
    let clearInput = $(".clear-input");

    $(document).keydown(function(e){
        if(e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
    }).keyup(function(e){
        if(e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
    });

    /**
     * ---------------------------
     * Blur search input on Esc
     * ---------------------------
     */
    body.on("keydown", function(e){
        // alert(e.which);
        if(e.which === 27){
            searchInput.blur();
        }
    });
    let autocompleteInitialized = false;
    let autocompleteInit = function(){
        autocompleteInitialized = true;
        searchInput.autocomplete({
            source: function(request, response){
                $.ajax({
                    url: "/api/translate/autocomplete",
                    data: {
                        lang_from: langFrom,
                        lang_to: langTo,
                        query: request.term
                    },
                    dataType: "json",
                    success: function(data){
                        response(data);
                    },
                });
            },
            minLength: 3,
            delay: 0,
            select: function(event, ui){
                searchInput.val(ui.item.value);
                translateBtn.trigger("click");
            }
        }).keyup(function(e){
            if(e.which === 13){
                $(".ui-menu-item").hide();
            }
        });
    };
    searchInput.on('keydown', function(e){
        if(!autocompleteInitialized){
            autocompleteInit();
        }
    });
    searchInput.eq(1).on('keydown blur', function(){
        topSearchInput.val(this.value);
    });

    let switchLang = function(selectedLang){
        $(".lang-div").addClass('jqk-hide');
        $("." + selectedLang + "-lang").removeClass('jqk-hide');
        let placeholder = selectedLang === 'kz' ? 'Сөзді немесе сөз тіркесін енгізіңіз' : 'Enter a word or phrase';
        let mainH1 = selectedLang === 'kz' ? 'Қазақ-ағылшын сөздігі' : 'English-kazakh dictionary';
        let translateBtn = selectedLang === 'kz' ? 'Аудару' : 'Translate';
        if(selectedLang === 'qaz'){
            placeholder = 'Sózdi nemese sóz tirkesin engizińiz';
            mainH1 = 'Qazaq-aǵylshyn sózdigi';
            translateBtn = 'Aýdarý';
        }
        searchInput.attr("placeholder", placeholder);
        $("#main_h1").text(mainH1);
        $(".random-phrases span").hide();
        $(".random-phrases-" + selectedLang).show();
        $(".translate-btn").text(translateBtn);
    };
    $(".switch-lang").on("click", function(){
        langFrom = $("select[name=lang_from]").val();
        langTo = $("select[name=lang_to]").val();
        $("select[name=lang_from]").val(langTo);
        $("select[name=lang_to]").val(langFrom);
        let t = langFrom;
        langFrom = langTo;
        langTo = t;
        switchLang(langFrom);
    });
    $("select[name=lang_from], select[name=lang_to]").on('change', function(){
        if($(this).attr('name') === 'lang_from'){
            langFrom = $(this).val();
            $("select[name=lang_from]").val(langFrom);
            langTo = langFrom === 'kz' || langFrom === 'qaz' ? 'en' : 'kz';
            $("select[name=lang_to]").val(langTo);
            switchLang(langFrom);
        }else{
            langTo = $(this).val();
            langFrom = langTo === 'kz' || langTo === 'qaz' ? 'en' : 'kz';
            $("select[name=lang_from]").val(langFrom);
            switchLang(langFrom);
        }
    });
    $(".translate-btn").on("click", function(){
        let query = searchInput.val();
        let resultRow = $('.result-row');
        let result = $('#result');
        let translateBtn = $('.translate-btn');
        searchInput.autocomplete("close");
        if(query !== ''){
            $.ajax({
                url: "/api/translate",
                data: {
                    lang_from: langFrom,
                    lang_to: langTo,
                    query: query,
                    suggests: 0
                },
                dataType: "json",
                beforeSend: function(){
                    resultRow.show();
                    result.addClass('loading');
                    translateBtn.attr('disabled', 'disabled');
                },
                success(response){
                    $("body").find(".show-history").show();
                    resultRow.hide();
                    result.removeClass('loading');
                    translateBtn.removeAttr('disabled');
                    if(response['success']){
                        result.html(response['html']);
                        resultRow.show();
                    }
                }
            });
        }
    });
    /**
     * ------------------------
     * Triggering click on Enter
     * ------------------------
     */
    searchInput.on("keydown", function(e){
        if(e.which === 13){
            translateBtn.trigger("click");
        }
    });
    clearInput.on("click", function(){
        searchInput.val('');
    });
    body.on("keypress", ".search-input", function(event){
        let key = String.fromCharCode(event.which);
        let alphabet = kazakhAlphabet;
        if(langFrom === 'en'){
            alphabet = englishAlphabet;
        }else if(langFrom === 'qaz'){
            alphabet = qazaqAlphabet;
        }
        if(event.keyCode === 8 || event.keyCode === 37 || event.keyCode === 39 || alphabet.test(key)){
            return true;
        }
        return false;
    });
    clearInput.on("click", function(){
        searchInput.val('');
        topSearchInput.val('');
    });
    $(".random-phrases").on("click", "a", function(e){
        let phrase = $(this).text();
        searchInput.val(phrase);
        translateBtn.trigger("click");
        e.preventDefault();
    });
    $('.keyboard-toggle').on('click', function(){
        let keyboardHolder = $(".keyboard-holder");
        let lang = $("html").attr("lang");
        let show = lang == 'en' ? "Show keyboard" : "Пернетақтаны көрсету";
        let hide = lang == 'en' ? "Hide keyboard" : "Пернетақтаны жасыру";
        let title = keyboardHolder.is(':visible') ? show : hide;
        let keyboardVisible = keyboardHolder.is(':visible') ? 'hidden' : 'visible';
        Cookies.set('keyboard', keyboardVisible, { path: '' });
        keyboardHolder.slideToggle();
        $(this).attr('title', title);
    });
    $("body").on("click", ".show-history", function(){
        autocompleteInitialized = false;
        searchInput.eq(1).autocomplete({
            source: function(request, response){
                $.ajax({
                    url: '/getRequestsHistory',
                    dataType: 'json',
                    success(data){
                        response(data);
                    }
                });
            },
            minLength: 0
        }).focus(function() {
            $(this).autocomplete("search", $(this).val());
        });
        searchInput.eq(1).focus();
    });
    jqKeyboard.init({
        containment: "#keyboard",
        allowed: [".search-input"]
    });
    autocompleteInit();
    searchInput.focus();
    switchLang(langFrom);
});