jQuery(function(){
    jQuery(document).keydown(function(e){
        if ((e.ctrlKey && e.keyCode == 13) || (e.metaKey && e.keyCode == 13)) {
            e.preventDefault();
            let text = "";
            if(window.getSelection){
                text = window.getSelection().toString();
            }else if(document.selection && document.selection.type != "Control"){
                text = document.selection.createRange().text;
            }
            let reportModal = $("#reportModal");
            reportModal.find('input[name=selected_text]').val(text);
            reportModal.find('.error-div').html('<div class="text-danger h4 mt-0 alert-danger">' + text + '</div>');
            reportModal.modal('show');
        }
    });
});