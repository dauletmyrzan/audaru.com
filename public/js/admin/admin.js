$(document).ready(function(){
    let body = $('body');
    body.on('click', '.delete-translated-word', function(){
        let thisWord = $(this).parent();
        let id = thisWord.data('id');
        if(confirm('Вы уверены, что хотите удалить слово?')){
            $.ajax({
                url: "/admin/translations/delete",
                type: "POST",
                data: {
                    id: id
                },
                success(response){
                    if(response['success']){
                        $.notify({
                            body: 'Перевод успешно удален!',
                            class: 'success'
                        });
                        thisWord.remove();
                    }
                }
            });
        }
    });
    body.on('click', '.edit-translated-word', function(){
        let modal = $("#editTranslationModal");
        let id = $(this).parent().data('id');
        $.ajax({
            url: '/admin/getEditForm',
            dataType: 'json',
            data: {
                id: id
            },
            success(response){
                modal.find('.modal-body').html(response['html']);
                modal.modal('show');
            }
        });
    });
    body.on('click', '#save_edit_translation_btn', function(){
        let data = $('form[name=edit_translation_form]').serialize();
        $.ajax({
            url: '/admin/translations/update',
            type: 'post',
            data: data,
            dataType: 'json',
            success(response){
                $.notify({
                    body: response['message'],
                    class: 'success'
                });
                $("#editTranslationModal").modal('hide');
            },
            error: function (err) {
                if (err.status == 422) { // when status code is 422, it's a validation issue
                    let body = '';
                    // display errors on each form field
                    $.each(err.responseJSON.errors, function (i, error) {
                        body += error[0] + "<br>";
                    });
                    $.notify({
                        body: body,
                        class: 'danger'
                    });
                    $("#editTranslationModal").modal('hide');
                }
            }
        });
    });
});