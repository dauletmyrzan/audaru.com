<?php
$start = microtime(true);
// Подключим файл с api
include('C:\sphinx-3.1.1\api\sphinxapi.php');

// Создадим объект - клиент сфинкса и подключимся к нашей службе
$cl = new SphinxClient();
$cl->SetServer("localhost", 9312);
$cl->SetLimits(0, 1000);

// Собственно поиск
$result = $cl->Query("the"); // поисковый запрос

// обработка результатов запроса
if($result === false)
{
	echo "Query failed: " . $cl->GetLastError() . ".\n"; // выводим ошибку если произошла
}
else
{
	if($cl->GetLastWarning())
	{
		echo "WARNING: " . $cl->GetLastWarning() . " // выводим предупреждение если оно было";
	}
	if(!empty($result["matches"]))
	{
		// если есть результаты поиска - обрабатываем их
		foreach($result["matches"] as $product => $info)
		{
			echo $product . "<br />"; // просто выводим id найденных товаров
		}
	}
}
$end = microtime(true);
echo "<br>";
echo round($end - $start, 6) . ' seconds';
exit;